FROM node:14-alpine

WORKDIR /app
ADD package.json /app 
ADD package-lock.json /app 
RUN npm install --silent
COPY ./src /app

EXPOSE 3000

ENV HOST=0.0.0.0
ENV PORT=3000

CMD [ "npm", "start" ]