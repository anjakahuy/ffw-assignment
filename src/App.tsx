/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Metric from "./pages/metric";
import QuickBooks from "./pages/quickbooks";
import SalesForce from "./pages/salesforce";

function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul  className="nav">
            <li className="nav-item">
              <Link className="nav-link"to="/salesforce">SalesForce</Link>
            </li>
            <li>
              <Link  className="nav-link" to="/quickbooks">QuickBooks</Link>
            </li>
            <li>
              <Link className="nav-link" to="/metric">Metric</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/salesforce">
            <SalesForce />
          </Route>
          <Route path="/quickbooks">
            <QuickBooks />
          </Route>
          <Route path="/metric">
            <Metric />
          </Route>
          <Route path="/">
            <SalesForce />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
