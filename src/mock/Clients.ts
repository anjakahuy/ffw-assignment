const CustomerBalanceDetail = {
  "Header": {
    "Time": "2021-06-18T06:26:24-07:00",
    "ReportName": "CustomerBalanceDetail",
    "DateMacro": "all",
    "Currency": "USD",
    "Option": [
      {
        "Name": "report_date",
        "Value": "2021-06-30"
      },
      {
        "Name": "NoReportData",
        "Value": "false"
      }
    ]
  },
  "Columns": {
    "Column": [
      {
        "ColTitle": "Date",
        "ColType": "Date",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "tx_date"
          }
        ]
      },
      {
        "ColTitle": "Transaction Type",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "txn_type"
          }
        ]
      },
      {
        "ColTitle": "Num",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "doc_num"
          }
        ]
      },
      {
        "ColTitle": "Due Date",
        "ColType": "Date",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "due_date"
          }
        ]
      },
      {
        "ColTitle": "Amount",
        "ColType": "Money",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "subt_amount"
          }
        ]
      },
      {
        "ColTitle": "Open Balance",
        "ColType": "Money",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "subt_open_bal"
          }
        ]
      },
      {
        "ColTitle": "Balance",
        "ColType": "Money",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "rbal_open_bal"
          }
        ]
      }
    ]
  },
  "Rows": {
    "Row": [
      {
        "Header": {
          "ColData": [
            {
              "value": "Amy's Bird Sanctuary",
              "id": "1"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            {
              "ColData": [
                {
                  "value": "2021-04-30"
                },
                {
                  "value": "Invoice",
                  "id": "67"
                },
                {
                  "value": "1021"
                },
                {
                  "value": "2021-05-30"
                },
                {
                  "value": "459.00"
                },
                {
                  "value": "239.00"
                },
                {
                  "value": "239.00"
                }
              ],
              "type": "Data"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Total for Amy's Bird Sanctuary"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "459.00"
            },
            {
              "value": "239.00"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Header": {
          "ColData": [
            {
              "value": "Bill's Windsurf Shop",
              "id": "2"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            {
              "ColData": [
                {
                  "value": "2021-04-04"
                },
                {
                  "value": "Invoice",
                  "id": "75"
                },
                {
                  "value": "1027"
                },
                {
                  "value": "2021-05-04"
                },
                {
                  "value": "85.00"
                },
                {
                  "value": "85.00"
                },
                {
                  "value": "85.00"
                }
              ],
              "type": "Data"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Total for Bill's Windsurf Shop"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "85.00"
            },
            {
              "value": "85.00"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Header": {
          "ColData": [
            {
              "value": "Freeman Sporting Goods"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            {
              "Header": {
                "ColData": [
                  {
                    "value": "0969 Ocean View Road",
                    "id": "8"
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  }
                ]
              },
              "Rows": {
                "Row": [
                  {
                    "ColData": [
                      {
                        "value": "2021-05-22"
                      },
                      {
                        "value": "Invoice",
                        "id": "129"
                      },
                      {
                        "value": "1036"
                      },
                      {
                        "value": "2021-06-21"
                      },
                      {
                        "value": "477.50"
                      },
                      {
                        "value": "477.50"
                      },
                      {
                        "value": "477.50"
                      }
                    ],
                    "type": "Data"
                  }
                ]
              },
              "Summary": {
                "ColData": [
                  {
                    "value": "Total for 0969 Ocean View Road"
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": "477.50"
                  },
                  {
                    "value": "477.50"
                  },
                  {
                    "value": ""
                  }
                ]
              },
              "type": "Section"
            },
            {
              "Header": {
                "ColData": [
                  {
                    "value": "55 Twin Lane",
                    "id": "9"
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  }
                ]
              },
              "Rows": {
                "Row": [
                  {
                    "ColData": [
                      {
                        "value": "2021-04-04"
                      },
                      {
                        "value": "Invoice",
                        "id": "92"
                      },
                      {
                        "value": "1028"
                      },
                      {
                        "value": "2021-05-04"
                      },
                      {
                        "value": "81.00"
                      },
                      {
                        "value": "81.00"
                      },
                      {
                        "value": "81.00"
                      }
                    ],
                    "type": "Data"
                  },
                  {
                    "ColData": [
                      {
                        "value": "2021-05-13"
                      },
                      {
                        "value": "Invoice",
                        "id": "13"
                      },
                      {
                        "value": "1005"
                      },
                      {
                        "value": "2021-06-12"
                      },
                      {
                        "value": "54.00"
                      },
                      {
                        "value": "4.00"
                      },
                      {
                        "value": "85.00"
                      }
                    ],
                    "type": "Data"
                  }
                ]
              },
              "Summary": {
                "ColData": [
                  {
                    "value": "Total for 55 Twin Lane"
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": "135.00"
                  },
                  {
                    "value": "85.00"
                  },
                  {
                    "value": ""
                  }
                ]
              },
              "type": "Section"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Total for Freeman Sporting Goods"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "612.50"
            },
            {
              "value": "562.50"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Header": {
          "ColData": [
            {
              "value": "Geeta Kalapatapu",
              "id": "10"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            {
              "ColData": [
                {
                  "value": "2021-05-21"
                },
                {
                  "value": "Invoice",
                  "id": "103"
                },
                {
                  "value": "1033"
                },
                {
                  "value": "2021-06-20"
                },
                {
                  "value": "629.10"
                },
                {
                  "value": "629.10"
                },
                {
                  "value": "629.10"
                }
              ],
              "type": "Data"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Total for Geeta Kalapatapu"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "629.10"
            },
            {
              "value": "629.10"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Header": {
          "ColData": [
            {
              "value": "Jeff's Jalopies",
              "id": "12"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            {
              "ColData": [
                {
                  "value": "2021-04-30"
                },
                {
                  "value": "Invoice",
                  "id": "68"
                },
                {
                  "value": "1022"
                },
                {
                  "value": "2021-05-30"
                },
                {
                  "value": "81.00"
                },
                {
                  "value": "81.00"
                },
                {
                  "value": "81.00"
                }
              ],
              "type": "Data"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Total for Jeff's Jalopies"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "81.00"
            },
            {
              "value": "81.00"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Header": {
          "ColData": [
            {
              "value": "John Melton",
              "id": "13"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            {
              "ColData": [
                {
                  "value": "2021-04-27"
                },
                {
                  "value": "Invoice",
                  "id": "16"
                },
                {
                  "value": "1007"
                },
                {
                  "value": "2021-05-27"
                },
                {
                  "value": "750.00"
                },
                {
                  "value": "450.00"
                },
                {
                  "value": "450.00"
                }
              ],
              "type": "Data"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Total for John Melton"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "750.00"
            },
            {
              "value": "450.00"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Header": {
          "ColData": [
            {
              "value": "Kookies by Kathy",
              "id": "16"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            {
              "ColData": [
                {
                  "value": "2021-04-03"
                },
                {
                  "value": "Invoice",
                  "id": "60"
                },
                {
                  "value": "1016"
                },
                {
                  "value": "2021-05-03"
                },
                {
                  "value": "75.00"
                },
                {
                  "value": "75.00"
                },
                {
                  "value": "75.00"
                }
              ],
              "type": "Data"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Total for Kookies by Kathy"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "75.00"
            },
            {
              "value": "75.00"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Header": {
          "ColData": [
            {
              "value": "Mark Cho",
              "id": "17"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            {
              "ColData": [
                {
                  "value": "2021-05-22"
                },
                {
                  "value": "Invoice",
                  "id": "119"
                },
                {
                  "value": "1035"
                },
                {
                  "value": "2021-06-21"
                },
                {
                  "value": "314.28"
                },
                {
                  "value": "314.28"
                },
                {
                  "value": "314.28"
                }
              ],
              "type": "Data"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Total for Mark Cho"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "314.28"
            },
            {
              "value": "314.28"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Header": {
          "ColData": [
            {
              "value": "Paulsen Medical Supplies",
              "id": "18"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            {
              "ColData": [
                {
                  "value": "2021-05-20"
                },
                {
                  "value": "Invoice",
                  "id": "49"
                },
                {
                  "value": "1015"
                },
                {
                  "value": "2021-06-19"
                },
                {
                  "value": "954.75"
                },
                {
                  "value": "954.75"
                },
                {
                  "value": "954.75"
                }
              ],
              "type": "Data"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Total for Paulsen Medical Supplies"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "954.75"
            },
            {
              "value": "954.75"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Header": {
          "ColData": [
            {
              "value": "Red Rock Diner",
              "id": "20"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            {
              "ColData": [
                {
                  "value": "2021-03-14"
                },
                {
                  "value": "Invoice",
                  "id": "70"
                },
                {
                  "value": "1024"
                },
                {
                  "value": "2021-04-13"
                },
                {
                  "value": "156.00"
                },
                {
                  "value": "156.00"
                },
                {
                  "value": "156.00"
                }
              ],
              "type": "Data"
            },
            {
              "ColData": [
                {
                  "value": "2021-05-20"
                },
                {
                  "value": "Invoice",
                  "id": "69"
                },
                {
                  "value": "1023"
                },
                {
                  "value": "2021-06-19"
                },
                {
                  "value": "70.00"
                },
                {
                  "value": "70.00"
                },
                {
                  "value": "226.00"
                }
              ],
              "type": "Data"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Total for Red Rock Diner"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "226.00"
            },
            {
              "value": "226.00"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Header": {
          "ColData": [
            {
              "value": "Rondonuwu Fruit and Vegi",
              "id": "21"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            {
              "ColData": [
                {
                  "value": "2021-05-21"
                },
                {
                  "value": "Invoice",
                  "id": "106"
                },
                {
                  "value": "1034"
                },
                {
                  "value": "2021-06-20"
                },
                {
                  "value": "78.60"
                },
                {
                  "value": "78.60"
                },
                {
                  "value": "78.60"
                }
              ],
              "type": "Data"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Total for Rondonuwu Fruit and Vegi"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "78.60"
            },
            {
              "value": "78.60"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Header": {
          "ColData": [
            {
              "value": "Shara Barnett"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            {
              "Header": {
                "ColData": [
                  {
                    "value": "Barnett Design",
                    "id": "23"
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  }
                ]
              },
              "Rows": {
                "Row": [
                  {
                    "ColData": [
                      {
                        "value": "2021-05-08"
                      },
                      {
                        "value": "Invoice",
                        "id": "39"
                      },
                      {
                        "value": "1012"
                      },
                      {
                        "value": "2021-06-07"
                      },
                      {
                        "value": "274.50"
                      },
                      {
                        "value": "274.50"
                      },
                      {
                        "value": "274.50"
                      }
                    ],
                    "type": "Data"
                  }
                ]
              },
              "Summary": {
                "ColData": [
                  {
                    "value": "Total for Barnett Design"
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": "274.50"
                  },
                  {
                    "value": "274.50"
                  },
                  {
                    "value": ""
                  }
                ]
              },
              "type": "Section"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Total for Shara Barnett"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "274.50"
            },
            {
              "value": "274.50"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Header": {
          "ColData": [
            {
              "value": "Sonnenschein Family Store",
              "id": "24"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            {
              "ColData": [
                {
                  "value": "2021-05-22"
                },
                {
                  "value": "Invoice",
                  "id": "130"
                },
                {
                  "value": "1037"
                },
                {
                  "value": "2021-06-21"
                },
                {
                  "value": "362.07"
                },
                {
                  "value": "362.07"
                },
                {
                  "value": "362.07"
                }
              ],
              "type": "Data"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Total for Sonnenschein Family Store"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "362.07"
            },
            {
              "value": "362.07"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Header": {
          "ColData": [
            {
              "value": "Sushi by Katsuyuki",
              "id": "25"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            {
              "ColData": [
                {
                  "value": "2021-05-13"
                },
                {
                  "value": "Invoice",
                  "id": "64"
                },
                {
                  "value": "1018"
                },
                {
                  "value": "2021-06-12"
                },
                {
                  "value": "80.00"
                },
                {
                  "value": "80.00"
                },
                {
                  "value": "80.00"
                }
              ],
              "type": "Data"
            },
            {
              "ColData": [
                {
                  "value": "2021-05-20"
                },
                {
                  "value": "Invoice",
                  "id": "65"
                },
                {
                  "value": "1019"
                },
                {
                  "value": "2021-06-19"
                },
                {
                  "value": "80.00"
                },
                {
                  "value": "80.00"
                },
                {
                  "value": "160.00"
                }
              ],
              "type": "Data"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Total for Sushi by Katsuyuki"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "160.00"
            },
            {
              "value": "160.00"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Header": {
          "ColData": [
            {
              "value": "Travis Waldron",
              "id": "26"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            {
              "ColData": [
                {
                  "value": "2021-05-19"
                },
                {
                  "value": "Invoice",
                  "id": "99"
                },
                {
                  "value": "1032"
                },
                {
                  "value": "2021-06-18"
                },
                {
                  "value": "414.72"
                },
                {
                  "value": "414.72"
                },
                {
                  "value": "414.72"
                }
              ],
              "type": "Data"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Total for Travis Waldron"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "414.72"
            },
            {
              "value": "414.72"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Header": {
          "ColData": [
            {
              "value": "Weiskopf Consulting",
              "id": "29"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            {
              "ColData": [
                {
                  "value": "2021-05-20"
                },
                {
                  "value": "Invoice",
                  "id": "34"
                },
                {
                  "value": "1010"
                },
                {
                  "value": "2021-06-19"
                },
                {
                  "value": "375.00"
                },
                {
                  "value": "375.00"
                },
                {
                  "value": "375.00"
                }
              ],
              "type": "Data"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Total for Weiskopf Consulting"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "375.00"
            },
            {
              "value": "375.00"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Summary": {
          "ColData": [
            {
              "value": "TOTAL"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "5851.52"
            },
            {
              "value": "5281.52"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      }
    ]
  }
}

export default CustomerBalanceDetail