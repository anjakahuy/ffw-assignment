const ProfitAndLossDetail: { 
  Header: any;
  Columns: any;
  Rows: any;
} = {
  "Header": {
    "Time": "2021-06-18T01:49:19-07:00",
    "ReportName": "ProfitAndLossDetail",
    "DateMacro": "this calendar year-to-date",
    "ReportBasis": "Accrual",
    "StartPeriod": "2021-01-01",
    "EndPeriod": "2021-06-18",
    "Currency": "USD",
    "Option": [
      {
        "Name": "NoReportData",
        "Value": "false"
      }
    ]
  },
  "Columns": {
    "Column": [
      {
        "ColTitle": "Date",
        "ColType": "Date",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "tx_date"
          }
        ]
      },
      {
        "ColTitle": "Transaction Type",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "txn_type"
          }
        ]
      },
      {
        "ColTitle": "Num",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "doc_num"
          }
        ]
      },
      {
        "ColTitle": "Name",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "name"
          }
        ]
      },
      {
        "ColTitle": "Memo/Description",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "memo"
          }
        ]
      },
      {
        "ColTitle": "Split",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "split_acc"
          }
        ]
      },
      {
        "ColTitle": "Amount",
        "ColType": "Money",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "subt_nat_amount"
          }
        ]
      },
      {
        "ColTitle": "Balance",
        "ColType": "Money",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "rbal_nat_amount"
          }
        ]
      }
    ]
  },
  "Rows": {
    "Row": [
      {
        "Header": {
          "ColData": [
            {
              "value": "Ordinary Income/Expenses"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            
            {
              "Header": {
                "ColData": [
                  {
                    "value": "Income"
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  }
                ]
              },
              "Rows": {
                "Row": [
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Design income",
                          "id": "82"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "ColData": [
                            {
                              "value": "2021-04-27"
                            },
                            {
                              "value": "Sales Receipt",
                              "id": "17"
                            },
                            {
                              "value": "1008"
                            },
                            {
                              "value": "Kate Whelan",
                              "id": "14"
                            },
                            {
                              "value": "Custom Design"
                            },
                            {
                              "value": "Checking",
                              "id": "35"
                            },
                            {
                              "value": "225.00"
                            },
                            {
                              "value": "225.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-04-27"
                            },
                            {
                              "value": "Invoice",
                              "id": "16"
                            },
                            {
                              "value": "1007"
                            },
                            {
                              "value": "John Melton",
                              "id": "13"
                            },
                            {
                              "value": "Custom Design"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "750.00"
                            },
                            {
                              "value": "975.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-17"
                            },
                            {
                              "value": "Sales Receipt",
                              "id": "11"
                            },
                            {
                              "value": "1003"
                            },
                            {
                              "value": "Dylan Sollfrank",
                              "id": "6"
                            },
                            {
                              "value": "Custom Design"
                            },
                            {
                              "value": "Checking",
                              "id": "35"
                            },
                            {
                              "value": "337.50"
                            },
                            {
                              "value": "1312.50"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-20"
                            },
                            {
                              "value": "Invoice",
                              "id": "34"
                            },
                            {
                              "value": "1010"
                            },
                            {
                              "value": "Weiskopf Consulting",
                              "id": "29"
                            },
                            {
                              "value": "Custom Design"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "375.00"
                            },
                            {
                              "value": "1687.50"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-20"
                            },
                            {
                              "value": "Invoice",
                              "id": "49"
                            },
                            {
                              "value": "1015"
                            },
                            {
                              "value": "Paulsen Medical Supplies",
                              "id": "18"
                            },
                            {
                              "value": "Custom Design"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "300.00"
                            },
                            {
                              "value": "1987.50"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-21"
                            },
                            {
                              "value": "Invoice",
                              "id": "103"
                            },
                            {
                              "value": "1033"
                            },
                            {
                              "value": "Geeta Kalapatapu",
                              "id": "10"
                            },
                            {
                              "value": "Custom Design"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "262.50"
                            },
                            {
                              "value": "2250.00"
                            }
                          ],
                          "type": "Data"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Design income"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "2250.00"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  },
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Discounts given",
                          "id": "86"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "ColData": [
                            {
                              "value": "2021-05-08"
                            },
                            {
                              "value": "Invoice",
                              "id": "39"
                            },
                            {
                              "value": "1012"
                            },
                            {
                              "value": "Shara Barnett:Barnett Design",
                              "id": "23"
                            },
                            {
                              "value": "Discount"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "-30.50"
                            },
                            {
                              "value": "-30.50"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-20"
                            },
                            {
                              "value": "Invoice",
                              "id": "49"
                            },
                            {
                              "value": "1015"
                            },
                            {
                              "value": "Paulsen Medical Supplies",
                              "id": "18"
                            },
                            {
                              "value": "Discount"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "-50.25"
                            },
                            {
                              "value": "-80.75"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-20"
                            },
                            {
                              "value": "Sales Receipt",
                              "id": "38"
                            },
                            {
                              "value": "1011"
                            },
                            {
                              "value": "Pye's Cakes",
                              "id": "15"
                            },
                            {
                              "value": "Discount"
                            },
                            {
                              "value": "Undeposited Funds",
                              "id": "4"
                            },
                            {
                              "value": "-8.75"
                            },
                            {
                              "value": "-89.50"
                            }
                          ],
                          "type": "Data"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Discounts given"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "-89.50"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  },
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Landscaping Services",
                          "id": "45"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "Rows": {
                            "Row": [
                              {
                                "ColData": [
                                  {
                                    "value": "2021-02-04"
                                  },
                                  {
                                    "value": "Invoice",
                                    "id": "95"
                                  },
                                  {
                                    "value": "1030"
                                  },
                                  {
                                    "value": "Freeman Sporting Goods:0969 Ocean View Road",
                                    "id": "8"
                                  },
                                  {
                                    "value": "Weekly Gardening Service"
                                  },
                                  {
                                    "value": "Accounts Receivable (A/R)",
                                    "id": "84"
                                  },
                                  {
                                    "value": "50.00"
                                  },
                                  {
                                    "value": "50.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-02-04"
                                  },
                                  {
                                    "value": "Invoice",
                                    "id": "10"
                                  },
                                  {
                                    "value": "1002"
                                  },
                                  {
                                    "value": "Bill's Windsurf Shop",
                                    "id": "2"
                                  },
                                  {
                                    "value": "Weekly Gardening Service"
                                  },
                                  {
                                    "value": "Accounts Receivable (A/R)",
                                    "id": "84"
                                  },
                                  {
                                    "value": "140.00"
                                  },
                                  {
                                    "value": "190.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-03-07"
                                  },
                                  {
                                    "value": "Invoice",
                                    "id": "96"
                                  },
                                  {
                                    "value": "1031"
                                  },
                                  {
                                    "value": "Freeman Sporting Goods:0969 Ocean View Road",
                                    "id": "8"
                                  },
                                  {
                                    "value": "Weekly Gardening Service"
                                  },
                                  {
                                    "value": "Accounts Receivable (A/R)",
                                    "id": "84"
                                  },
                                  {
                                    "value": "90.00"
                                  },
                                  {
                                    "value": "280.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-04-03"
                                  },
                                  {
                                    "value": "Invoice",
                                    "id": "60"
                                  },
                                  {
                                    "value": "1016"
                                  },
                                  {
                                    "value": "Kookies by Kathy",
                                    "id": "16"
                                  },
                                  {
                                    "value": "Weekly Gardening Service"
                                  },
                                  {
                                    "value": "Accounts Receivable (A/R)",
                                    "id": "84"
                                  },
                                  {
                                    "value": "75.00"
                                  },
                                  {
                                    "value": "355.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-04-04"
                                  },
                                  {
                                    "value": "Invoice",
                                    "id": "92"
                                  },
                                  {
                                    "value": "1028"
                                  },
                                  {
                                    "value": "Freeman Sporting Goods:55 Twin Lane",
                                    "id": "9"
                                  },
                                  {
                                    "value": "Weekly Gardening Service"
                                  },
                                  {
                                    "value": "Accounts Receivable (A/R)",
                                    "id": "84"
                                  },
                                  {
                                    "value": "75.00"
                                  },
                                  {
                                    "value": "430.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-04-04"
                                  },
                                  {
                                    "value": "Invoice",
                                    "id": "71"
                                  },
                                  {
                                    "value": "1025"
                                  },
                                  {
                                    "value": "Amy's Bird Sanctuary",
                                    "id": "1"
                                  },
                                  {
                                    "value": "Weekly Gardening Service"
                                  },
                                  {
                                    "value": "Accounts Receivable (A/R)",
                                    "id": "84"
                                  },
                                  {
                                    "value": "120.00"
                                  },
                                  {
                                    "value": "550.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-04-04"
                                  },
                                  {
                                    "value": "Invoice",
                                    "id": "75"
                                  },
                                  {
                                    "value": "1027"
                                  },
                                  {
                                    "value": "Bill's Windsurf Shop",
                                    "id": "2"
                                  },
                                  {
                                    "value": "Weekly Gardening Service"
                                  },
                                  {
                                    "value": "Accounts Receivable (A/R)",
                                    "id": "84"
                                  },
                                  {
                                    "value": "50.00"
                                  },
                                  {
                                    "value": "600.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-04-13"
                                  },
                                  {
                                    "value": "Invoice",
                                    "id": "14"
                                  },
                                  {
                                    "value": "1006"
                                  },
                                  {
                                    "value": "Freeman Sporting Goods:55 Twin Lane",
                                    "id": "9"
                                  },
                                  {
                                    "value": "Weekly Gardening Service"
                                  },
                                  {
                                    "value": "Accounts Receivable (A/R)",
                                    "id": "84"
                                  },
                                  {
                                    "value": "80.00"
                                  },
                                  {
                                    "value": "680.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-04-30"
                                  },
                                  {
                                    "value": "Invoice",
                                    "id": "68"
                                  },
                                  {
                                    "value": "1022"
                                  },
                                  {
                                    "value": "Jeff's Jalopies",
                                    "id": "12"
                                  },
                                  {
                                    "value": "Weekly Gardening Service"
                                  },
                                  {
                                    "value": "Accounts Receivable (A/R)",
                                    "id": "84"
                                  },
                                  {
                                    "value": "75.00"
                                  },
                                  {
                                    "value": "755.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-06"
                                  },
                                  {
                                    "value": "Invoice",
                                    "id": "63"
                                  },
                                  {
                                    "value": "1017"
                                  },
                                  {
                                    "value": "Sushi by Katsuyuki",
                                    "id": "25"
                                  },
                                  {
                                    "value": "Weekly Gardening Service"
                                  },
                                  {
                                    "value": "Accounts Receivable (A/R)",
                                    "id": "84"
                                  },
                                  {
                                    "value": "80.00"
                                  },
                                  {
                                    "value": "835.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-10"
                                  },
                                  {
                                    "value": "Invoice",
                                    "id": "42"
                                  },
                                  {
                                    "value": "1013"
                                  },
                                  {
                                    "value": "Travis Waldron",
                                    "id": "26"
                                  },
                                  {
                                    "value": "Weekly Gardening Service"
                                  },
                                  {
                                    "value": "Accounts Receivable (A/R)",
                                    "id": "84"
                                  },
                                  {
                                    "value": "75.00"
                                  },
                                  {
                                    "value": "910.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-13"
                                  },
                                  {
                                    "value": "Invoice",
                                    "id": "13"
                                  },
                                  {
                                    "value": "1005"
                                  },
                                  {
                                    "value": "Freeman Sporting Goods:55 Twin Lane",
                                    "id": "9"
                                  },
                                  {
                                    "value": "Weekly Gardening Service"
                                  },
                                  {
                                    "value": "Accounts Receivable (A/R)",
                                    "id": "84"
                                  },
                                  {
                                    "value": "50.00"
                                  },
                                  {
                                    "value": "960.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-13"
                                  },
                                  {
                                    "value": "Invoice",
                                    "id": "64"
                                  },
                                  {
                                    "value": "1018"
                                  },
                                  {
                                    "value": "Sushi by Katsuyuki",
                                    "id": "25"
                                  },
                                  {
                                    "value": "Weekly Gardening Service"
                                  },
                                  {
                                    "value": "Accounts Receivable (A/R)",
                                    "id": "84"
                                  },
                                  {
                                    "value": "80.00"
                                  },
                                  {
                                    "value": "1040.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-18"
                                  },
                                  {
                                    "value": "Sales Receipt",
                                    "id": "47"
                                  },
                                  {
                                    "value": "1014"
                                  },
                                  {
                                    "value": "Diego Rodriguez",
                                    "id": "4"
                                  },
                                  {
                                    "value": "Weekly Gardening Service"
                                  },
                                  {
                                    "value": "Undeposited Funds",
                                    "id": "4"
                                  },
                                  {
                                    "value": "140.00"
                                  },
                                  {
                                    "value": "1180.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-19"
                                  },
                                  {
                                    "value": "Invoice",
                                    "id": "9"
                                  },
                                  {
                                    "value": "1001"
                                  },
                                  {
                                    "value": "Amy's Bird Sanctuary",
                                    "id": "1"
                                  },
                                  {
                                    "value": "Weekly Gardening Service"
                                  },
                                  {
                                    "value": "Accounts Receivable (A/R)",
                                    "id": "84"
                                  },
                                  {
                                    "value": "100.00"
                                  },
                                  {
                                    "value": "1280.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-20"
                                  },
                                  {
                                    "value": "Invoice",
                                    "id": "65"
                                  },
                                  {
                                    "value": "1019"
                                  },
                                  {
                                    "value": "Sushi by Katsuyuki",
                                    "id": "25"
                                  },
                                  {
                                    "value": "Weekly Gardening Service"
                                  },
                                  {
                                    "value": "Accounts Receivable (A/R)",
                                    "id": "84"
                                  },
                                  {
                                    "value": "80.00"
                                  },
                                  {
                                    "value": "1360.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-21"
                                  },
                                  {
                                    "value": "Invoice",
                                    "id": "106"
                                  },
                                  {
                                    "value": "1034"
                                  },
                                  {
                                    "value": "Rondonuwu Fruit and Vegi",
                                    "id": "21"
                                  },
                                  {
                                    "value": "Tree and Shrub Trimming"
                                  },
                                  {
                                    "value": "Accounts Receivable (A/R)",
                                    "id": "84"
                                  },
                                  {
                                    "value": "30.00"
                                  },
                                  {
                                    "value": "1390.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-22"
                                  },
                                  {
                                    "value": "Invoice",
                                    "id": "129"
                                  },
                                  {
                                    "value": "1036"
                                  },
                                  {
                                    "value": "Freeman Sporting Goods:0969 Ocean View Road",
                                    "id": "8"
                                  },
                                  {
                                    "value": "Weekly Gardening Service"
                                  },
                                  {
                                    "value": "Accounts Receivable (A/R)",
                                    "id": "84"
                                  },
                                  {
                                    "value": "87.50"
                                  },
                                  {
                                    "value": "1477.50"
                                  }
                                ],
                                "type": "Data"
                              }
                            ]
                          },
                          "Summary": {
                            "ColData": [
                              {
                                "value": "Total for Landscaping Services"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": "1477.50"
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "type": "Section"
                        },
                        {
                          "Header": {
                            "ColData": [
                              {
                                "value": "Job Materials"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "Rows": {
                            "Row": [
                              {
                                "Header": {
                                  "ColData": [
                                    {
                                      "value": "Fountains and Garden Lighting",
                                      "id": "48"
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    }
                                  ]
                                },
                                "Rows": {
                                  "Row": [
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-03-07"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "96"
                                        },
                                        {
                                          "value": "1031"
                                        },
                                        {
                                          "value": "Freeman Sporting Goods:0969 Ocean View Road",
                                          "id": "8"
                                        },
                                        {
                                          "value": "Rock Fountain"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "275.00"
                                        },
                                        {
                                          "value": "275.00"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-03-14"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "70"
                                        },
                                        {
                                          "value": "1024"
                                        },
                                        {
                                          "value": "Red Rock Diner",
                                          "id": "20"
                                        },
                                        {
                                          "value": "Garden Rocks"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "48.00"
                                        },
                                        {
                                          "value": "323.00"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-04-04"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "93"
                                        },
                                        {
                                          "value": "1029"
                                        },
                                        {
                                          "value": "Dukes Basketball Camp",
                                          "id": "5"
                                        },
                                        {
                                          "value": "Garden Rocks"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "72.00"
                                        },
                                        {
                                          "value": "395.00"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-04-04"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "93"
                                        },
                                        {
                                          "value": "1029"
                                        },
                                        {
                                          "value": "Dukes Basketball Camp",
                                          "id": "5"
                                        },
                                        {
                                          "value": "Rock Fountain"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "275.00"
                                        },
                                        {
                                          "value": "670.00"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-04-04"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "93"
                                        },
                                        {
                                          "value": "1029"
                                        },
                                        {
                                          "value": "Dukes Basketball Camp",
                                          "id": "5"
                                        },
                                        {
                                          "value": "Concrete for fountain installation"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "75.00"
                                        },
                                        {
                                          "value": "745.00"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-04-30"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "67"
                                        },
                                        {
                                          "value": "1021"
                                        },
                                        {
                                          "value": "Amy's Bird Sanctuary",
                                          "id": "1"
                                        },
                                        {
                                          "value": "Rock Fountain"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "275.00"
                                        },
                                        {
                                          "value": "1020.00"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-08"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "39"
                                        },
                                        {
                                          "value": "1012"
                                        },
                                        {
                                          "value": "Shara Barnett:Barnett Design",
                                          "id": "23"
                                        },
                                        {
                                          "value": "Rock Fountain"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "275.00"
                                        },
                                        {
                                          "value": "1295.00"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-19"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "99"
                                        },
                                        {
                                          "value": "1032"
                                        },
                                        {
                                          "value": "Travis Waldron",
                                          "id": "26"
                                        },
                                        {
                                          "value": "Garden Rocks"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "84.00"
                                        },
                                        {
                                          "value": "1379.00"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-20"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "49"
                                        },
                                        {
                                          "value": "1015"
                                        },
                                        {
                                          "value": "Paulsen Medical Supplies",
                                          "id": "18"
                                        },
                                        {
                                          "value": "Garden Rocks"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "180.00"
                                        },
                                        {
                                          "value": "1559.00"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-20"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "49"
                                        },
                                        {
                                          "value": "1015"
                                        },
                                        {
                                          "value": "Paulsen Medical Supplies",
                                          "id": "18"
                                        },
                                        {
                                          "value": "Rock Fountain"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "275.00"
                                        },
                                        {
                                          "value": "1834.00"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-21"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "106"
                                        },
                                        {
                                          "value": "1034"
                                        },
                                        {
                                          "value": "Rondonuwu Fruit and Vegi",
                                          "id": "21"
                                        },
                                        {
                                          "value": "Garden Lighting"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "45.00"
                                        },
                                        {
                                          "value": "1879.00"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-21"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "103"
                                        },
                                        {
                                          "value": "1033"
                                        },
                                        {
                                          "value": "Geeta Kalapatapu",
                                          "id": "10"
                                        },
                                        {
                                          "value": "Fountain Pump"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "45.00"
                                        },
                                        {
                                          "value": "1924.00"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-21"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "103"
                                        },
                                        {
                                          "value": "1033"
                                        },
                                        {
                                          "value": "Geeta Kalapatapu",
                                          "id": "10"
                                        },
                                        {
                                          "value": "Rock Fountain"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "275.00"
                                        },
                                        {
                                          "value": "2199.00"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-22"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "130"
                                        },
                                        {
                                          "value": "1037"
                                        },
                                        {
                                          "value": "Sonnenschein Family Store",
                                          "id": "24"
                                        },
                                        {
                                          "value": "Concrete for fountain installation"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "47.50"
                                        },
                                        {
                                          "value": "2246.50"
                                        }
                                      ],
                                      "type": "Data"
                                    }
                                  ]
                                },
                                "Summary": {
                                  "ColData": [
                                    {
                                      "value": "Total for Fountains and Garden Lighting"
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": "2246.50"
                                    },
                                    {
                                      "value": ""
                                    }
                                  ]
                                },
                                "type": "Section"
                              },
                              {
                                "Header": {
                                  "ColData": [
                                    {
                                      "value": "Plants and Soil",
                                      "id": "49"
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    }
                                  ]
                                },
                                "Rows": {
                                  "Row": [
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-02-04"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "95"
                                        },
                                        {
                                          "value": "1030"
                                        },
                                        {
                                          "value": "Freeman Sporting Goods:0969 Ocean View Road",
                                          "id": "8"
                                        },
                                        {
                                          "value": "Sod"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "131.25"
                                        },
                                        {
                                          "value": "131.25"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-04-30"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "67"
                                        },
                                        {
                                          "value": "1021"
                                        },
                                        {
                                          "value": "Amy's Bird Sanctuary",
                                          "id": "1"
                                        },
                                        {
                                          "value": "2 cubic ft. bag"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "150.00"
                                        },
                                        {
                                          "value": "281.25"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-04"
                                        },
                                        {
                                          "value": "Expense",
                                          "id": "80"
                                        },
                                        {
                                          "value": "8"
                                        },
                                        {
                                          "value": "Hicks Hardware",
                                          "id": "41"
                                        },
                                        {
                                          "value": ""
                                        },
                                        {
                                          "value": "Checking",
                                          "id": "35"
                                        },
                                        {
                                          "value": "-24.36"
                                        },
                                        {
                                          "value": "256.89"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-10"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "12"
                                        },
                                        {
                                          "value": "1004"
                                        },
                                        {
                                          "value": "Cool Cars",
                                          "id": "3"
                                        },
                                        {
                                          "value": "Sod"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "1750.00"
                                        },
                                        {
                                          "value": "2006.89"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-12"
                                        },
                                        {
                                          "value": "Expense",
                                          "id": "88"
                                        },
                                        {
                                          "value": "22"
                                        },
                                        {
                                          "value": "Tania's Nursery",
                                          "id": "50"
                                        },
                                        {
                                          "value": ""
                                        },
                                        {
                                          "value": "Mastercard",
                                          "id": "41"
                                        },
                                        {
                                          "value": "-54.92"
                                        },
                                        {
                                          "value": "1951.97"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-19"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "99"
                                        },
                                        {
                                          "value": "1032"
                                        },
                                        {
                                          "value": "Travis Waldron",
                                          "id": "26"
                                        },
                                        {
                                          "value": "Sod"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "300.00"
                                        },
                                        {
                                          "value": "2251.97"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-22"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "129"
                                        },
                                        {
                                          "value": "1036"
                                        },
                                        {
                                          "value": "Freeman Sporting Goods:0969 Ocean View Road",
                                          "id": "8"
                                        },
                                        {
                                          "value": "2 cubic ft. bag"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "50.00"
                                        },
                                        {
                                          "value": "2301.97"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-22"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "129"
                                        },
                                        {
                                          "value": "1036"
                                        },
                                        {
                                          "value": "Freeman Sporting Goods:0969 Ocean View Road",
                                          "id": "8"
                                        },
                                        {
                                          "value": "Sod"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "50.00"
                                        },
                                        {
                                          "value": "2351.97"
                                        }
                                      ],
                                      "type": "Data"
                                    }
                                  ]
                                },
                                "Summary": {
                                  "ColData": [
                                    {
                                      "value": "Total for Plants and Soil"
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": "2351.97"
                                    },
                                    {
                                      "value": ""
                                    }
                                  ]
                                },
                                "type": "Section"
                              },
                              {
                                "Header": {
                                  "ColData": [
                                    {
                                      "value": "Sprinklers and Drip Systems",
                                      "id": "50"
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    }
                                  ]
                                },
                                "Rows": {
                                  "Row": [
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-03-14"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "70"
                                        },
                                        {
                                          "value": "1024"
                                        },
                                        {
                                          "value": "Red Rock Diner",
                                          "id": "20"
                                        },
                                        {
                                          "value": "Sprinkler Pipes"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "60.00"
                                        },
                                        {
                                          "value": "60.00"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-03-14"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "70"
                                        },
                                        {
                                          "value": "1024"
                                        },
                                        {
                                          "value": "Red Rock Diner",
                                          "id": "20"
                                        },
                                        {
                                          "value": "Sprinkler Pipes"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "48.00"
                                        },
                                        {
                                          "value": "108.00"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-08"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "39"
                                        },
                                        {
                                          "value": "1012"
                                        },
                                        {
                                          "value": "Shara Barnett:Barnett Design",
                                          "id": "23"
                                        },
                                        {
                                          "value": "Sprinkler Heads"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "30.00"
                                        },
                                        {
                                          "value": "138.00"
                                        }
                                      ],
                                      "type": "Data"
                                    }
                                  ]
                                },
                                "Summary": {
                                  "ColData": [
                                    {
                                      "value": "Total for Sprinklers and Drip Systems"
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": "138.00"
                                    },
                                    {
                                      "value": ""
                                    }
                                  ]
                                },
                                "type": "Section"
                              }
                            ]
                          },
                          "Summary": {
                            "ColData": [
                              {
                                "value": "Total for Job Materials"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": "4736.47"
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "type": "Section"
                        },
                        {
                          "Header": {
                            "ColData": [
                              {
                                "value": "Labor"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "Rows": {
                            "Row": [
                              {
                                "Header": {
                                  "ColData": [
                                    {
                                      "value": "Installation",
                                      "id": "52"
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    }
                                  ]
                                },
                                "Rows": {
                                  "Row": [
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-20"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "49"
                                        },
                                        {
                                          "value": "1015"
                                        },
                                        {
                                          "value": "Paulsen Medical Supplies",
                                          "id": "18"
                                        },
                                        {
                                          "value": "Installation of landscape design"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "250.00"
                                        },
                                        {
                                          "value": "250.00"
                                        }
                                      ],
                                      "type": "Data"
                                    }
                                  ]
                                },
                                "Summary": {
                                  "ColData": [
                                    {
                                      "value": "Total for Installation"
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": "250.00"
                                    },
                                    {
                                      "value": ""
                                    }
                                  ]
                                },
                                "type": "Section"
                              },
                              {
                                "Header": {
                                  "ColData": [
                                    {
                                      "value": "Maintenance and Repair",
                                      "id": "53"
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    }
                                  ]
                                },
                                "Rows": {
                                  "Row": [
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-04-04"
                                        },
                                        {
                                          "value": "Invoice",
                                          "id": "71"
                                        },
                                        {
                                          "value": "1025"
                                        },
                                        {
                                          "value": "Amy's Bird Sanctuary",
                                          "id": "1"
                                        },
                                        {
                                          "value": "Maintenance & Repair"
                                        },
                                        {
                                          "value": "Accounts Receivable (A/R)",
                                          "id": "84"
                                        },
                                        {
                                          "value": "50.00"
                                        },
                                        {
                                          "value": "50.00"
                                        }
                                      ],
                                      "type": "Data"
                                    }
                                  ]
                                },
                                "Summary": {
                                  "ColData": [
                                    {
                                      "value": "Total for Maintenance and Repair"
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": "50.00"
                                    },
                                    {
                                      "value": ""
                                    }
                                  ]
                                },
                                "type": "Section"
                              }
                            ]
                          },
                          "Summary": {
                            "ColData": [
                              {
                                "value": "Total for Labor"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": "300.00"
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "type": "Section"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Landscaping Services with sub-accounts"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "6513.97"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  },
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Pest Control Services",
                          "id": "54"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "ColData": [
                            {
                              "value": "2021-02-04"
                            },
                            {
                              "value": "Invoice",
                              "id": "10"
                            },
                            {
                              "value": "1002"
                            },
                            {
                              "value": "Bill's Windsurf Shop",
                              "id": "2"
                            },
                            {
                              "value": "Pest Control Services"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "35.00"
                            },
                            {
                              "value": "35.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-02-04"
                            },
                            {
                              "value": "Invoice",
                              "id": "95"
                            },
                            {
                              "value": "1030"
                            },
                            {
                              "value": "Freeman Sporting Goods:0969 Ocean View Road",
                              "id": "8"
                            },
                            {
                              "value": "Pest Control Services"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "35.00"
                            },
                            {
                              "value": "70.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-04-04"
                            },
                            {
                              "value": "Invoice",
                              "id": "75"
                            },
                            {
                              "value": "1027"
                            },
                            {
                              "value": "Bill's Windsurf Shop",
                              "id": "2"
                            },
                            {
                              "value": "Pest Control Services"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "35.00"
                            },
                            {
                              "value": "105.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-04-04"
                            },
                            {
                              "value": "Invoice",
                              "id": "71"
                            },
                            {
                              "value": "1025"
                            },
                            {
                              "value": "Amy's Bird Sanctuary",
                              "id": "1"
                            },
                            {
                              "value": "Pest Control Services"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "35.00"
                            },
                            {
                              "value": "140.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-05"
                            },
                            {
                              "value": "Credit Memo",
                              "id": "73"
                            },
                            {
                              "value": "1026"
                            },
                            {
                              "value": "Amy's Bird Sanctuary",
                              "id": "1"
                            },
                            {
                              "value": "Pest Control Services"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "-100.00"
                            },
                            {
                              "value": "40.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-20"
                            },
                            {
                              "value": "Sales Receipt",
                              "id": "38"
                            },
                            {
                              "value": "1011"
                            },
                            {
                              "value": "Pye's Cakes",
                              "id": "15"
                            },
                            {
                              "value": "Pest Control Services"
                            },
                            {
                              "value": "Undeposited Funds",
                              "id": "4"
                            },
                            {
                              "value": "87.50"
                            },
                            {
                              "value": "127.50"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-20"
                            },
                            {
                              "value": "Refund",
                              "id": "66"
                            },
                            {
                              "value": "1020"
                            },
                            {
                              "value": "Pye's Cakes",
                              "id": "15"
                            },
                            {
                              "value": "Refund - Pest control was ineffective"
                            },
                            {
                              "value": "Checking",
                              "id": "35"
                            },
                            {
                              "value": "-87.50"
                            },
                            {
                              "value": "40.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-20"
                            },
                            {
                              "value": "Invoice",
                              "id": "69"
                            },
                            {
                              "value": "1023"
                            },
                            {
                              "value": "Red Rock Diner",
                              "id": "20"
                            },
                            {
                              "value": "Pest Control Services"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "70.00"
                            },
                            {
                              "value": "110.00"
                            }
                          ],
                          "type": "Data"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Pest Control Services"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "110.00"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  },
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Sales of Product Income",
                          "id": "79"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "ColData": [
                            {
                              "value": "2021-05-10"
                            },
                            {
                              "value": "Invoice",
                              "id": "12"
                            },
                            {
                              "value": "1004"
                            },
                            {
                              "value": "Cool Cars",
                              "id": "3"
                            },
                            {
                              "value": "Sprinkler Heads"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "20.00"
                            },
                            {
                              "value": "20.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-10"
                            },
                            {
                              "value": "Invoice",
                              "id": "12"
                            },
                            {
                              "value": "1004"
                            },
                            {
                              "value": "Cool Cars",
                              "id": "3"
                            },
                            {
                              "value": "Sprinkler Pipes"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "24.00"
                            },
                            {
                              "value": "44.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-22"
                            },
                            {
                              "value": "Invoice",
                              "id": "130"
                            },
                            {
                              "value": "1037"
                            },
                            {
                              "value": "Sonnenschein Family Store",
                              "id": "24"
                            },
                            {
                              "value": "Fountain Pump"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "12.75"
                            },
                            {
                              "value": "56.75"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-22"
                            },
                            {
                              "value": "Invoice",
                              "id": "130"
                            },
                            {
                              "value": "1037"
                            },
                            {
                              "value": "Sonnenschein Family Store",
                              "id": "24"
                            },
                            {
                              "value": "Rock Fountain"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "275.00"
                            },
                            {
                              "value": "331.75"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-22"
                            },
                            {
                              "value": "Invoice",
                              "id": "129"
                            },
                            {
                              "value": "1036"
                            },
                            {
                              "value": "Freeman Sporting Goods:0969 Ocean View Road",
                              "id": "8"
                            },
                            {
                              "value": "Fountain Pump"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "15.00"
                            },
                            {
                              "value": "346.75"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-22"
                            },
                            {
                              "value": "Invoice",
                              "id": "129"
                            },
                            {
                              "value": "1036"
                            },
                            {
                              "value": "Freeman Sporting Goods:0969 Ocean View Road",
                              "id": "8"
                            },
                            {
                              "value": "Rock Fountain"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "275.00"
                            },
                            {
                              "value": "621.75"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-22"
                            },
                            {
                              "value": "Invoice",
                              "id": "119"
                            },
                            {
                              "value": "1035"
                            },
                            {
                              "value": "Mark Cho",
                              "id": "17"
                            },
                            {
                              "value": "Rock Fountain"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "275.00"
                            },
                            {
                              "value": "896.75"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-22"
                            },
                            {
                              "value": "Invoice",
                              "id": "119"
                            },
                            {
                              "value": "1035"
                            },
                            {
                              "value": "Mark Cho",
                              "id": "17"
                            },
                            {
                              "value": "Sprinkler Pipes"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "16.00"
                            },
                            {
                              "value": "912.75"
                            }
                          ],
                          "type": "Data"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Sales of Product Income"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "912.75"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  },
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Services",
                          "id": "1"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "ColData": [
                            {
                              "value": "2021-05-10"
                            },
                            {
                              "value": "Invoice",
                              "id": "12"
                            },
                            {
                              "value": "1004"
                            },
                            {
                              "value": "Cool Cars",
                              "id": "3"
                            },
                            {
                              "value": "Installation Hours"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "400.00"
                            },
                            {
                              "value": "400.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-19"
                            },
                            {
                              "value": "Invoice",
                              "id": "27"
                            },
                            {
                              "value": "1009"
                            },
                            {
                              "value": "Travis Waldron",
                              "id": "26"
                            },
                            {
                              "value": "Lumber"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "103.55"
                            },
                            {
                              "value": "503.55"
                            }
                          ],
                          "type": "Data"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Services"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "503.55"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  }
                ]
              },
              "Summary": {
                "ColData": [
                  {
                    "value": "Total for Income"
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": "10200.77"
                  },
                  {
                    "value": ""
                  }
                ]
              },
              "type": "Section"
            },
            {
              "Header": {
                "ColData": [
                  {
                    "value": "Cost of Goods Sold"
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  }
                ]
              },
              "Rows": {
                "Row": [
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Cost of Goods Sold",
                          "id": "80"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "ColData": [
                            {
                              "value": "2021-05-22"
                            },
                            {
                              "value": "Invoice",
                              "id": "119"
                            },
                            {
                              "value": "1035"
                            },
                            {
                              "value": "Mark Cho",
                              "id": "17"
                            },
                            {
                              "value": "Sprinkler Pipes"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "10.00"
                            },
                            {
                              "value": "10.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-22"
                            },
                            {
                              "value": "Invoice",
                              "id": "129"
                            },
                            {
                              "value": "1036"
                            },
                            {
                              "value": "Freeman Sporting Goods:0969 Ocean View Road",
                              "id": "8"
                            },
                            {
                              "value": "Fountain Pump"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "10.00"
                            },
                            {
                              "value": "20.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-22"
                            },
                            {
                              "value": "Invoice",
                              "id": "119"
                            },
                            {
                              "value": "1035"
                            },
                            {
                              "value": "Mark Cho",
                              "id": "17"
                            },
                            {
                              "value": "Rock Fountain"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "125.00"
                            },
                            {
                              "value": "145.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-22"
                            },
                            {
                              "value": "Invoice",
                              "id": "130"
                            },
                            {
                              "value": "1037"
                            },
                            {
                              "value": "Sonnenschein Family Store",
                              "id": "24"
                            },
                            {
                              "value": "Fountain Pump"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "10.00"
                            },
                            {
                              "value": "155.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-22"
                            },
                            {
                              "value": "Invoice",
                              "id": "130"
                            },
                            {
                              "value": "1037"
                            },
                            {
                              "value": "Sonnenschein Family Store",
                              "id": "24"
                            },
                            {
                              "value": "Rock Fountain"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "125.00"
                            },
                            {
                              "value": "280.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-22"
                            },
                            {
                              "value": "Invoice",
                              "id": "129"
                            },
                            {
                              "value": "1036"
                            },
                            {
                              "value": "Freeman Sporting Goods:0969 Ocean View Road",
                              "id": "8"
                            },
                            {
                              "value": "Rock Fountain"
                            },
                            {
                              "value": "Accounts Receivable (A/R)",
                              "id": "84"
                            },
                            {
                              "value": "125.00"
                            },
                            {
                              "value": "405.00"
                            }
                          ],
                          "type": "Data"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Cost of Goods Sold"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "405.00"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  }
                ]
              },
              "Summary": {
                "ColData": [
                  {
                    "value": "Total for Cost of Goods Sold"
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": "405.00"
                  },
                  {
                    "value": ""
                  }
                ]
              },
              "type": "Section"
            },
            {
              "Header": {
                "ColData": [
                  {
                    "value": "Expenses"
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  }
                ]
              },
              "Rows": {
                "Row": [
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Advertising",
                          "id": "7"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "ColData": [
                            {
                              "value": "2021-05-20"
                            },
                            {
                              "value": "Expense",
                              "id": "55"
                            },
                            {
                              "value": ""
                            },
                            {
                              "value": "Lee Advertising",
                              "id": "42"
                            },
                            {
                              "value": ""
                            },
                            {
                              "value": "Mastercard",
                              "id": "41"
                            },
                            {
                              "value": "74.86"
                            },
                            {
                              "value": "74.86"
                            }
                          ],
                          "type": "Data"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Advertising"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "74.86"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  },
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Automobile",
                          "id": "55"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "Rows": {
                            "Row": [
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-13"
                                  },
                                  {
                                    "value": "Cash Expense",
                                    "id": "136"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Squeaky Kleen Car Wash",
                                    "id": "57"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Checking",
                                    "id": "35"
                                  },
                                  {
                                    "value": "19.99"
                                  },
                                  {
                                    "value": "19.99"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-20"
                                  },
                                  {
                                    "value": "Check",
                                    "id": "138"
                                  },
                                  {
                                    "value": "Debit"
                                  },
                                  {
                                    "value": "Squeaky Kleen Car Wash",
                                    "id": "57"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Checking",
                                    "id": "35"
                                  },
                                  {
                                    "value": "19.99"
                                  },
                                  {
                                    "value": "39.98"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-27"
                                  },
                                  {
                                    "value": "Credit Card Expense",
                                    "id": "141"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Squeaky Kleen Car Wash",
                                    "id": "57"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Mastercard",
                                    "id": "41"
                                  },
                                  {
                                    "value": "19.99"
                                  },
                                  {
                                    "value": "59.97"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-06-03"
                                  },
                                  {
                                    "value": "Credit Card Expense",
                                    "id": "142"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Squeaky Kleen Car Wash",
                                    "id": "57"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Mastercard",
                                    "id": "41"
                                  },
                                  {
                                    "value": "19.99"
                                  },
                                  {
                                    "value": "79.96"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-06-16"
                                  },
                                  {
                                    "value": "Credit Card Expense",
                                    "id": "144"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "",
                                    "id": ""
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Mastercard",
                                    "id": "41"
                                  },
                                  {
                                    "value": "34.00"
                                  },
                                  {
                                    "value": "113.96"
                                  }
                                ],
                                "type": "Data"
                              }
                            ]
                          },
                          "Summary": {
                            "ColData": [
                              {
                                "value": "Total for Automobile"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": "113.96"
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "type": "Section"
                        },
                        {
                          "Header": {
                            "ColData": [
                              {
                                "value": "Fuel",
                                "id": "56"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "Rows": {
                            "Row": [
                              {
                                "ColData": [
                                  {
                                    "value": "2021-04-02"
                                  },
                                  {
                                    "value": "Check",
                                    "id": "57"
                                  },
                                  {
                                    "value": "4"
                                  },
                                  {
                                    "value": "Chin's Gas and Oil",
                                    "id": "33"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Checking",
                                    "id": "35"
                                  },
                                  {
                                    "value": "54.55"
                                  },
                                  {
                                    "value": "54.55"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-04-17"
                                  },
                                  {
                                    "value": "Check",
                                    "id": "58"
                                  },
                                  {
                                    "value": "5"
                                  },
                                  {
                                    "value": "Chin's Gas and Oil",
                                    "id": "33"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Checking",
                                    "id": "35"
                                  },
                                  {
                                    "value": "62.01"
                                  },
                                  {
                                    "value": "116.56"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-07"
                                  },
                                  {
                                    "value": "Expense",
                                    "id": "59"
                                  },
                                  {
                                    "value": "1"
                                  },
                                  {
                                    "value": "Chin's Gas and Oil",
                                    "id": "33"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Mastercard",
                                    "id": "41"
                                  },
                                  {
                                    "value": "65.00"
                                  },
                                  {
                                    "value": "181.56"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-14"
                                  },
                                  {
                                    "value": "Cash Expense",
                                    "id": "132"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Chin's Gas and Oil",
                                    "id": "33"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Checking",
                                    "id": "35"
                                  },
                                  {
                                    "value": "52.14"
                                  },
                                  {
                                    "value": "233.70"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-21"
                                  },
                                  {
                                    "value": "Cash Expense",
                                    "id": "131"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Chin's Gas and Oil",
                                    "id": "33"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Checking",
                                    "id": "35"
                                  },
                                  {
                                    "value": "63.15"
                                  },
                                  {
                                    "value": "296.85"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-22"
                                  },
                                  {
                                    "value": "Expense",
                                    "id": "122"
                                  },
                                  {
                                    "value": "1"
                                  },
                                  {
                                    "value": "Chin's Gas and Oil",
                                    "id": "33"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Mastercard",
                                    "id": "41"
                                  },
                                  {
                                    "value": "52.56"
                                  },
                                  {
                                    "value": "349.41"
                                  }
                                ],
                                "type": "Data"
                              }
                            ]
                          },
                          "Summary": {
                            "ColData": [
                              {
                                "value": "Total for Fuel"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": "349.41"
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "type": "Section"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Automobile with sub-accounts"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "463.37"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  },
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Equipment Rental",
                          "id": "29"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "ColData": [
                            {
                              "value": "2021-05-20"
                            },
                            {
                              "value": "Expense",
                              "id": "51"
                            },
                            {
                              "value": "1"
                            },
                            {
                              "value": "Ellis Equipment Rental",
                              "id": "38"
                            },
                            {
                              "value": "Equipment rental for 5 days"
                            },
                            {
                              "value": "Mastercard",
                              "id": "41"
                            },
                            {
                              "value": "112.00"
                            },
                            {
                              "value": "112.00"
                            }
                          ],
                          "type": "Data"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Equipment Rental"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "112.00"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  },
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Insurance",
                          "id": "11"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "ColData": [
                            {
                              "value": "2021-05-15"
                            },
                            {
                              "value": "Bill",
                              "id": "19"
                            },
                            {
                              "value": ""
                            },
                            {
                              "value": "Brosnahan Insurance Agency",
                              "id": "31"
                            },
                            {
                              "value": ""
                            },
                            {
                              "value": "Accounts Payable (A/P)",
                              "id": "33"
                            },
                            {
                              "value": "241.23"
                            },
                            {
                              "value": "241.23"
                            }
                          ],
                          "type": "Data"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Insurance"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "241.23"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  },
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Job Expenses",
                          "id": "58"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "Rows": {
                            "Row": [
                              {
                                "ColData": [
                                  {
                                    "value": "2021-04-18"
                                  },
                                  {
                                    "value": "Expense",
                                    "id": "87"
                                  },
                                  {
                                    "value": "15"
                                  },
                                  {
                                    "value": "Tania's Nursery",
                                    "id": "50"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Checking",
                                    "id": "35"
                                  },
                                  {
                                    "value": "108.09"
                                  },
                                  {
                                    "value": "108.09"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-21"
                                  },
                                  {
                                    "value": "Expense",
                                    "id": "89"
                                  },
                                  {
                                    "value": "108"
                                  },
                                  {
                                    "value": "Tania's Nursery",
                                    "id": "50"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Checking",
                                    "id": "35"
                                  },
                                  {
                                    "value": "46.98"
                                  },
                                  {
                                    "value": "155.07"
                                  }
                                ],
                                "type": "Data"
                              }
                            ]
                          },
                          "Summary": {
                            "ColData": [
                              {
                                "value": "Total for Job Expenses"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": "155.07"
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "type": "Section"
                        },
                        {
                          "Header": {
                            "ColData": [
                              {
                                "value": "Job Materials"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "Rows": {
                            "Row": [
                              {
                                "Header": {
                                  "ColData": [
                                    {
                                      "value": "Decks and Patios",
                                      "id": "64"
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    }
                                  ]
                                },
                                "Rows": {
                                  "Row": [
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-10"
                                        },
                                        {
                                          "value": "Expense",
                                          "id": "81"
                                        },
                                        {
                                          "value": "1"
                                        },
                                        {
                                          "value": "Hicks Hardware",
                                          "id": "41"
                                        },
                                        {
                                          "value": ""
                                        },
                                        {
                                          "value": "Mastercard",
                                          "id": "41"
                                        },
                                        {
                                          "value": "88.09"
                                        },
                                        {
                                          "value": "88.09"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-19"
                                        },
                                        {
                                          "value": "Bill",
                                          "id": "25"
                                        },
                                        {
                                          "value": ""
                                        },
                                        {
                                          "value": "Norton Lumber and Building Materials",
                                          "id": "46"
                                        },
                                        {
                                          "value": "Lumber"
                                        },
                                        {
                                          "value": "Accounts Payable (A/P)",
                                          "id": "33"
                                        },
                                        {
                                          "value": "103.55"
                                        },
                                        {
                                          "value": "191.64"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-28"
                                        },
                                        {
                                          "value": "Credit Card Expense",
                                          "id": "143"
                                        },
                                        {
                                          "value": ""
                                        },
                                        {
                                          "value": "Hicks Hardware",
                                          "id": "41"
                                        },
                                        {
                                          "value": ""
                                        },
                                        {
                                          "value": "Mastercard",
                                          "id": "41"
                                        },
                                        {
                                          "value": "42.40"
                                        },
                                        {
                                          "value": "234.04"
                                        }
                                      ],
                                      "type": "Data"
                                    }
                                  ]
                                },
                                "Summary": {
                                  "ColData": [
                                    {
                                      "value": "Total for Decks and Patios"
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": "234.04"
                                    },
                                    {
                                      "value": ""
                                    }
                                  ]
                                },
                                "type": "Section"
                              },
                              {
                                "Header": {
                                  "ColData": [
                                    {
                                      "value": "Plants and Soil",
                                      "id": "66"
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    }
                                  ]
                                },
                                "Rows": {
                                  "Row": [
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-03-18"
                                        },
                                        {
                                          "value": "Expense",
                                          "id": "85"
                                        },
                                        {
                                          "value": "3"
                                        },
                                        {
                                          "value": "Tania's Nursery",
                                          "id": "50"
                                        },
                                        {
                                          "value": ""
                                        },
                                        {
                                          "value": "Mastercard",
                                          "id": "41"
                                        },
                                        {
                                          "value": "158.08"
                                        },
                                        {
                                          "value": "158.08"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-04-11"
                                        },
                                        {
                                          "value": "Expense",
                                          "id": "86"
                                        },
                                        {
                                          "value": "9"
                                        },
                                        {
                                          "value": "Tania's Nursery",
                                          "id": "50"
                                        },
                                        {
                                          "value": "Morning Glories and Sod"
                                        },
                                        {
                                          "value": "Checking",
                                          "id": "35"
                                        },
                                        {
                                          "value": "89.09"
                                        },
                                        {
                                          "value": "247.17"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-16"
                                        },
                                        {
                                          "value": "Expense",
                                          "id": "56"
                                        },
                                        {
                                          "value": "50"
                                        },
                                        {
                                          "value": "Tania's Nursery",
                                          "id": "50"
                                        },
                                        {
                                          "value": ""
                                        },
                                        {
                                          "value": "Mastercard",
                                          "id": "41"
                                        },
                                        {
                                          "value": "82.45"
                                        },
                                        {
                                          "value": "329.62"
                                        }
                                      ],
                                      "type": "Data"
                                    },
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-25"
                                        },
                                        {
                                          "value": "Cash Expense",
                                          "id": "134"
                                        },
                                        {
                                          "value": ""
                                        },
                                        {
                                          "value": "Tania's Nursery",
                                          "id": "50"
                                        },
                                        {
                                          "value": ""
                                        },
                                        {
                                          "value": "Checking",
                                          "id": "35"
                                        },
                                        {
                                          "value": "23.50"
                                        },
                                        {
                                          "value": "353.12"
                                        }
                                      ],
                                      "type": "Data"
                                    }
                                  ]
                                },
                                "Summary": {
                                  "ColData": [
                                    {
                                      "value": "Total for Plants and Soil"
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": "353.12"
                                    },
                                    {
                                      "value": ""
                                    }
                                  ]
                                },
                                "type": "Section"
                              },
                              {
                                "Header": {
                                  "ColData": [
                                    {
                                      "value": "Sprinklers and Drip Systems",
                                      "id": "67"
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    }
                                  ]
                                },
                                "Rows": {
                                  "Row": [
                                    {
                                      "ColData": [
                                        {
                                          "value": "2021-05-16"
                                        },
                                        {
                                          "value": "Expense",
                                          "id": "83"
                                        },
                                        {
                                          "value": "13"
                                        },
                                        {
                                          "value": "Hicks Hardware",
                                          "id": "41"
                                        },
                                        {
                                          "value": ""
                                        },
                                        {
                                          "value": "Checking",
                                          "id": "35"
                                        },
                                        {
                                          "value": "215.66"
                                        },
                                        {
                                          "value": "215.66"
                                        }
                                      ],
                                      "type": "Data"
                                    }
                                  ]
                                },
                                "Summary": {
                                  "ColData": [
                                    {
                                      "value": "Total for Sprinklers and Drip Systems"
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": ""
                                    },
                                    {
                                      "value": "215.66"
                                    },
                                    {
                                      "value": ""
                                    }
                                  ]
                                },
                                "type": "Section"
                              }
                            ]
                          },
                          "Summary": {
                            "ColData": [
                              {
                                "value": "Total for Job Materials"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": "802.82"
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "type": "Section"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Job Expenses with sub-accounts"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "957.89"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  },
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Legal & Professional Fees",
                          "id": "12"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "Rows": {
                            "Row": [
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-22"
                                  },
                                  {
                                    "value": "Expense",
                                    "id": "127"
                                  },
                                  {
                                    "value": "76"
                                  },
                                  {
                                    "value": "Pam Seitz",
                                    "id": "47"
                                  },
                                  {
                                    "value": "Counsel"
                                  },
                                  {
                                    "value": "Checking",
                                    "id": "35"
                                  },
                                  {
                                    "value": "75.00"
                                  },
                                  {
                                    "value": "75.00"
                                  }
                                ],
                                "type": "Data"
                              }
                            ]
                          },
                          "Summary": {
                            "ColData": [
                              {
                                "value": "Total for Legal & Professional Fees"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": "75.00"
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "type": "Section"
                        },
                        {
                          "Header": {
                            "ColData": [
                              {
                                "value": "Accounting",
                                "id": "69"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "Rows": {
                            "Row": [
                              {
                                "ColData": [
                                  {
                                    "value": "2021-03-11"
                                  },
                                  {
                                    "value": "Expense",
                                    "id": "107"
                                  },
                                  {
                                    "value": "12"
                                  },
                                  {
                                    "value": "Robertson & Associates",
                                    "id": "49"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Checking",
                                    "id": "35"
                                  },
                                  {
                                    "value": "250.00"
                                  },
                                  {
                                    "value": "250.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-08"
                                  },
                                  {
                                    "value": "Bill",
                                    "id": "18"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Books by Bessie",
                                    "id": "30"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Accounts Payable (A/P)",
                                    "id": "33"
                                  },
                                  {
                                    "value": "75.00"
                                  },
                                  {
                                    "value": "325.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-22"
                                  },
                                  {
                                    "value": "Bill",
                                    "id": "108"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Robertson & Associates",
                                    "id": "49"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Accounts Payable (A/P)",
                                    "id": "33"
                                  },
                                  {
                                    "value": "315.00"
                                  },
                                  {
                                    "value": "640.00"
                                  }
                                ],
                                "type": "Data"
                              }
                            ]
                          },
                          "Summary": {
                            "ColData": [
                              {
                                "value": "Total for Accounting"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": "640.00"
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "type": "Section"
                        },
                        {
                          "Header": {
                            "ColData": [
                              {
                                "value": "Bookkeeper",
                                "id": "70"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "Rows": {
                            "Row": [
                              {
                                "ColData": [
                                  {
                                    "value": "2021-04-11"
                                  },
                                  {
                                    "value": "Check",
                                    "id": "109"
                                  },
                                  {
                                    "value": "12"
                                  },
                                  {
                                    "value": "Books by Bessie",
                                    "id": "30"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Checking",
                                    "id": "35"
                                  },
                                  {
                                    "value": "55.00"
                                  },
                                  {
                                    "value": "55.00"
                                  }
                                ],
                                "type": "Data"
                              }
                            ]
                          },
                          "Summary": {
                            "ColData": [
                              {
                                "value": "Total for Bookkeeper"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": "55.00"
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "type": "Section"
                        },
                        {
                          "Header": {
                            "ColData": [
                              {
                                "value": "Lawyer",
                                "id": "71"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "Rows": {
                            "Row": [
                              {
                                "ColData": [
                                  {
                                    "value": "2021-01-18"
                                  },
                                  {
                                    "value": "Bill",
                                    "id": "90"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Robertson & Associates",
                                    "id": "49"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Accounts Payable (A/P)",
                                    "id": "33"
                                  },
                                  {
                                    "value": "300.00"
                                  },
                                  {
                                    "value": "300.00"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-13"
                                  },
                                  {
                                    "value": "Check",
                                    "id": "84"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Tony Rondonuwu",
                                    "id": "52"
                                  },
                                  {
                                    "value": "Consulting"
                                  },
                                  {
                                    "value": "Checking",
                                    "id": "35"
                                  },
                                  {
                                    "value": "100.00"
                                  },
                                  {
                                    "value": "400.00"
                                  }
                                ],
                                "type": "Data"
                              }
                            ]
                          },
                          "Summary": {
                            "ColData": [
                              {
                                "value": "Total for Lawyer"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": "400.00"
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "type": "Section"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Legal & Professional Fees with sub-accounts"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "1170.00"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  },
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Maintenance and Repair",
                          "id": "72"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "Rows": {
                            "Row": [
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-14"
                                  },
                                  {
                                    "value": "Check",
                                    "id": "133"
                                  },
                                  {
                                    "value": "70"
                                  },
                                  {
                                    "value": "Chin's Gas and Oil",
                                    "id": "33"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Checking",
                                    "id": "35"
                                  },
                                  {
                                    "value": "185.00"
                                  },
                                  {
                                    "value": "185.00"
                                  }
                                ],
                                "type": "Data"
                              }
                            ]
                          },
                          "Summary": {
                            "ColData": [
                              {
                                "value": "Total for Maintenance and Repair"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": "185.00"
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "type": "Section"
                        },
                        {
                          "Header": {
                            "ColData": [
                              {
                                "value": "Equipment Repairs",
                                "id": "75"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "Rows": {
                            "Row": [
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-20"
                                  },
                                  {
                                    "value": "Bill",
                                    "id": "44"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Diego's Road Warrior Bodyshop",
                                    "id": "36"
                                  },
                                  {
                                    "value": "Repairs on the truck"
                                  },
                                  {
                                    "value": "Accounts Payable (A/P)",
                                    "id": "33"
                                  },
                                  {
                                    "value": "755.00"
                                  },
                                  {
                                    "value": "755.00"
                                  }
                                ],
                                "type": "Data"
                              }
                            ]
                          },
                          "Summary": {
                            "ColData": [
                              {
                                "value": "Total for Equipment Repairs"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": "755.00"
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "type": "Section"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Maintenance and Repair with sub-accounts"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "940.00"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  },
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Meals and Entertainment",
                          "id": "13"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "ColData": [
                            {
                              "value": "2021-05-13"
                            },
                            {
                              "value": "Cash Expense",
                              "id": "135"
                            },
                            {
                              "value": ""
                            },
                            {
                              "value": "Bob's Burger Joint",
                              "id": "56"
                            },
                            {
                              "value": ""
                            },
                            {
                              "value": "Checking",
                              "id": "35"
                            },
                            {
                              "value": "5.66"
                            },
                            {
                              "value": "5.66"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-18"
                            },
                            {
                              "value": "Cash Expense",
                              "id": "137"
                            },
                            {
                              "value": ""
                            },
                            {
                              "value": "Bob's Burger Joint",
                              "id": "56"
                            },
                            {
                              "value": ""
                            },
                            {
                              "value": "Checking",
                              "id": "35"
                            },
                            {
                              "value": "3.86"
                            },
                            {
                              "value": "9.52"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-27"
                            },
                            {
                              "value": "Credit Card Expense",
                              "id": "140"
                            },
                            {
                              "value": ""
                            },
                            {
                              "value": "Bob's Burger Joint",
                              "id": "56"
                            },
                            {
                              "value": ""
                            },
                            {
                              "value": "Mastercard",
                              "id": "41"
                            },
                            {
                              "value": "18.97"
                            },
                            {
                              "value": "28.49"
                            }
                          ],
                          "type": "Data"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Meals and Entertainment"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "28.49"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  },
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Office Expenses",
                          "id": "15"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "ColData": [
                            {
                              "value": "2021-05-16"
                            },
                            {
                              "value": "Check",
                              "id": "36"
                            },
                            {
                              "value": "2"
                            },
                            {
                              "value": "Mahoney Mugs",
                              "id": "43"
                            },
                            {
                              "value": "Office Supplies"
                            },
                            {
                              "value": "Checking",
                              "id": "35"
                            },
                            {
                              "value": "18.08"
                            },
                            {
                              "value": "18.08"
                            }
                          ],
                          "type": "Data"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Office Expenses"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "18.08"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  },
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Rent or Lease",
                          "id": "17"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "ColData": [
                            {
                              "value": "2021-05-06"
                            },
                            {
                              "value": "Bill",
                              "id": "24"
                            },
                            {
                              "value": ""
                            },
                            {
                              "value": "Hall Properties",
                              "id": "40"
                            },
                            {
                              "value": "Building Lease"
                            },
                            {
                              "value": "Accounts Payable (A/P)",
                              "id": "33"
                            },
                            {
                              "value": "900.00"
                            },
                            {
                              "value": "900.00"
                            }
                          ],
                          "type": "Data"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Rent or Lease"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "900.00"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  },
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Utilities"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "Header": {
                            "ColData": [
                              {
                                "value": "Gas and Electric",
                                "id": "76"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "Rows": {
                            "Row": [
                              {
                                "ColData": [
                                  {
                                    "value": "2021-04-07"
                                  },
                                  {
                                    "value": "Bill",
                                    "id": "78"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "PG&E",
                                    "id": "48"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Accounts Payable (A/P)",
                                    "id": "33"
                                  },
                                  {
                                    "value": "86.44"
                                  },
                                  {
                                    "value": "86.44"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-06"
                                  },
                                  {
                                    "value": "Bill",
                                    "id": "35"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "PG&E",
                                    "id": "48"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Accounts Payable (A/P)",
                                    "id": "33"
                                  },
                                  {
                                    "value": "114.09"
                                  },
                                  {
                                    "value": "200.53"
                                  }
                                ],
                                "type": "Data"
                              }
                            ]
                          },
                          "Summary": {
                            "ColData": [
                              {
                                "value": "Total for Gas and Electric"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": "200.53"
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "type": "Section"
                        },
                        {
                          "Header": {
                            "ColData": [
                              {
                                "value": "Telephone",
                                "id": "77"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "Rows": {
                            "Row": [
                              {
                                "ColData": [
                                  {
                                    "value": "2021-04-06"
                                  },
                                  {
                                    "value": "Bill",
                                    "id": "105"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Cal Telephone",
                                    "id": "32"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Accounts Payable (A/P)",
                                    "id": "33"
                                  },
                                  {
                                    "value": "56.50"
                                  },
                                  {
                                    "value": "56.50"
                                  }
                                ],
                                "type": "Data"
                              },
                              {
                                "ColData": [
                                  {
                                    "value": "2021-05-06"
                                  },
                                  {
                                    "value": "Bill",
                                    "id": "21"
                                  },
                                  {
                                    "value": ""
                                  },
                                  {
                                    "value": "Cal Telephone",
                                    "id": "32"
                                  },
                                  {
                                    "value": "Monthly Phone Bill"
                                  },
                                  {
                                    "value": "Accounts Payable (A/P)",
                                    "id": "33"
                                  },
                                  {
                                    "value": "74.36"
                                  },
                                  {
                                    "value": "130.86"
                                  }
                                ],
                                "type": "Data"
                              }
                            ]
                          },
                          "Summary": {
                            "ColData": [
                              {
                                "value": "Total for Telephone"
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": ""
                              },
                              {
                                "value": "130.86"
                              },
                              {
                                "value": ""
                              }
                            ]
                          },
                          "type": "Section"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Utilities"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "331.39"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  }
                ]
              },
              "Summary": {
                "ColData": [
                  {
                    "value": "Total for Expenses"
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": "5237.31"
                  },
                  {
                    "value": ""
                  }
                ]
              },
              "type": "Section"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Net Ordinary Income"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "4558.46"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Header": {
          "ColData": [
            {
              "value": "Other Income/Expense"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            }
          ]
        },
        "Rows": {
          "Row": [
            {
              "Header": {
                "ColData": [
                  {
                    "value": "Other Expense"
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  }
                ]
              },
              "Rows": {
                "Row": [
                  {
                    "Header": {
                      "ColData": [
                        {
                          "value": "Miscellaneous",
                          "id": "14"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "Rows": {
                      "Row": [
                        {
                          "ColData": [
                            {
                              "value": "2021-04-15"
                            },
                            {
                              "value": "Bill",
                              "id": "2"
                            },
                            {
                              "value": ""
                            },
                            {
                              "value": "Hicks Hardware",
                              "id": "41"
                            },
                            {
                              "value": ""
                            },
                            {
                              "value": "Accounts Payable (A/P)",
                              "id": "33"
                            },
                            {
                              "value": "250.00"
                            },
                            {
                              "value": "250.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-07"
                            },
                            {
                              "value": "Bill",
                              "id": "3"
                            },
                            {
                              "value": ""
                            },
                            {
                              "value": "Tim Philip Masonry",
                              "id": "51"
                            },
                            {
                              "value": ""
                            },
                            {
                              "value": "Accounts Payable (A/P)",
                              "id": "33"
                            },
                            {
                              "value": "666.00"
                            },
                            {
                              "value": "916.00"
                            }
                          ],
                          "type": "Data"
                        },
                        {
                          "ColData": [
                            {
                              "value": "2021-05-15"
                            },
                            {
                              "value": "Bill",
                              "id": "1"
                            },
                            {
                              "value": ""
                            },
                            {
                              "value": "Brosnahan Insurance Agency",
                              "id": "31"
                            },
                            {
                              "value": ""
                            },
                            {
                              "value": "Accounts Payable (A/P)",
                              "id": "33"
                            },
                            {
                              "value": "2000.00"
                            },
                            {
                              "value": "2916.00"
                            }
                          ],
                          "type": "Data"
                        }
                      ]
                    },
                    "Summary": {
                      "ColData": [
                        {
                          "value": "Total for Miscellaneous"
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": ""
                        },
                        {
                          "value": "2916.00"
                        },
                        {
                          "value": ""
                        }
                      ]
                    },
                    "type": "Section"
                  }
                ]
              },
              "Summary": {
                "ColData": [
                  {
                    "value": "Total for Other Expense"
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": ""
                  },
                  {
                    "value": "2916.00"
                  },
                  {
                    "value": ""
                  }
                ]
              },
              "type": "Section"
            }
          ]
        },
        "Summary": {
          "ColData": [
            {
              "value": "Net Other Income"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "-2916.00"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      },
      {
        "Summary": {
          "ColData": [
            {
              "value": "Net Income"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "1642.46"
            },
            {
              "value": ""
            }
          ]
        },
        "type": "Section"
      }
    ]
  }
}

export default ProfitAndLossDetail