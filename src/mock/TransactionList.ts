const TransactionList = {
  "Header": {
    "Time": "2021-06-18T06:35:03-07:00",
    "ReportName": "TransactionList",
    "DateMacro": "this month-to-date",
    "StartPeriod": "2021-06-01",
    "EndPeriod": "2021-06-18",
    "Currency": "USD",
    "Option": [
      {
        "Name": "NoReportData",
        "Value": "false"
      }
    ]
  },
  "Columns": {
    "Column": [
      {
        "ColTitle": "Date",
        "ColType": "Date",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "tx_date"
          }
        ]
      },
      {
        "ColTitle": "Transaction Type",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "txn_type"
          }
        ]
      },
      {
        "ColTitle": "Num",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "doc_num"
          }
        ]
      },
      {
        "ColTitle": "Posting",
        "ColType": "Boolean",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "is_no_post"
          }
        ]
      },
      {
        "ColTitle": "Name",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "name"
          }
        ]
      },
      {
        "ColTitle": "Memo/Description",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "memo"
          }
        ]
      },
      {
        "ColTitle": "Account",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "account_name"
          }
        ]
      },
      {
        "ColTitle": "Split",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "other_account"
          }
        ]
      },
      {
        "ColTitle": "Amount",
        "ColType": "Money",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "subt_nat_amount"
          }
        ]
      }
    ]
  },
  "Rows": {
    "Row": [
      {
        "ColData": [
          {
            "value": "2021-06-03"
          },
          {
            "value": "Credit Card Expense",
            "id": "142"
          },
          {
            "value": ""
          },
          {
            "value": "Yes"
          },
          {
            "value": "Squeaky Kleen Car Wash",
            "id": "57"
          },
          {
            "value": ""
          },
          {
            "value": "Mastercard",
            "id": "41"
          },
          {
            "value": "Automobile",
            "id": "55"
          },
          {
            "value": "19.99"
          }
        ],
        "type": "Data"
      },
      {
        "ColData": [
          {
            "value": "2021-06-05"
          },
          {
            "value": "Credit Card Credit",
            "id": "139"
          },
          {
            "value": ""
          },
          {
            "value": "Yes"
          },
          {
            "value": "",
            "id": ""
          },
          {
            "value": "Monthly Payment"
          },
          {
            "value": "Mastercard",
            "id": "41"
          },
          {
            "value": "Checking",
            "id": "35"
          },
          {
            "value": "-900.00"
          }
        ],
        "type": "Data"
      },
      {
        "ColData": [
          {
            "value": "2021-06-16"
          },
          {
            "value": "Credit Card Expense",
            "id": "144"
          },
          {
            "value": ""
          },
          {
            "value": "Yes"
          },
          {
            "value": "",
            "id": ""
          },
          {
            "value": ""
          },
          {
            "value": "Mastercard",
            "id": "41"
          },
          {
            "value": "Automobile",
            "id": "55"
          },
          {
            "value": "34.00"
          }
        ],
        "type": "Data"
      }
    ]
  }
}

export default TransactionList