const JournalReport = {
  "Header": {
    "Time": "2021-06-18T06:38:25-07:00",
    "ReportName": "JournalReport",
    "DateMacro": "this month-to-date",
    "StartPeriod": "2021-06-01",
    "EndPeriod": "2021-06-18",
    "Currency": "USD",
    "Option": [
      {
        "Name": "NoReportData",
        "Value": "false"
      }
    ]
  },
  "Columns": {
    "Column": [
      {
        "ColTitle": "Date",
        "ColType": "Date",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "tx_date"
          }
        ]
      },
      {
        "ColTitle": "Transaction Type",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "txn_type"
          }
        ]
      },
      {
        "ColTitle": "Num",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "doc_num"
          }
        ]
      },
      {
        "ColTitle": "Name",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "name"
          }
        ]
      },
      {
        "ColTitle": "Memo/Description",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "memo"
          }
        ]
      },
      {
        "ColTitle": "Account",
        "ColType": "String",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "account_name"
          }
        ]
      },
      {
        "ColTitle": "Debit",
        "ColType": "Money",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "debt_amt"
          }
        ]
      },
      {
        "ColTitle": "Credit",
        "ColType": "Money",
        "MetaData": [
          {
            "Name": "ColKey",
            "Value": "credit_amt"
          }
        ]
      }
    ]
  },
  "Rows": {
    "Row": [
      {
        "ColData": [
          {
            "value": "2021-06-03"
          },
          {
            "value": "Credit Card Expense",
            "id": "142"
          },
          {
            "value": ""
          },
          {
            "value": "Squeaky Kleen Car Wash",
            "id": "57"
          },
          {
            "value": ""
          },
          {
            "value": "Mastercard",
            "id": "41"
          },
          {
            "value": ""
          },
          {
            "value": "19.99"
          }
        ],
        "type": "Data"
      },
      {
        "ColData": [
          {
            "value": "0-00-00"
          },
          {
            "value": "",
            "id": "142"
          },
          {
            "value": ""
          },
          {
            "value": "",
            "id": ""
          },
          {
            "value": ""
          },
          {
            "value": "Automobile",
            "id": "55"
          },
          {
            "value": "19.99"
          },
          {
            "value": ""
          }
        ],
        "type": "Data"
      },
      {
        "Summary": {
          "ColData": [
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "19.99"
            },
            {
              "value": "19.99"
            }
          ]
        },
        "type": "Section"
      },
      {
        "ColData": [
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          }
        ],
        "type": "Data"
      },
      {
        "ColData": [
          {
            "value": "2021-06-05"
          },
          {
            "value": "Credit Card Credit",
            "id": "139"
          },
          {
            "value": ""
          },
          {
            "value": "",
            "id": ""
          },
          {
            "value": "Monthly Payment"
          },
          {
            "value": "Mastercard",
            "id": "41"
          },
          {
            "value": "900.00"
          },
          {
            "value": ""
          }
        ],
        "type": "Data"
      },
      {
        "ColData": [
          {
            "value": "0-00-00"
          },
          {
            "value": "",
            "id": "139"
          },
          {
            "value": ""
          },
          {
            "value": "",
            "id": ""
          },
          {
            "value": ""
          },
          {
            "value": "Checking",
            "id": "35"
          },
          {
            "value": ""
          },
          {
            "value": "900.00"
          }
        ],
        "type": "Data"
      },
      {
        "Summary": {
          "ColData": [
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "900.00"
            },
            {
              "value": "900.00"
            }
          ]
        },
        "type": "Section"
      },
      {
        "ColData": [
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          }
        ],
        "type": "Data"
      },
      {
        "ColData": [
          {
            "value": "2021-06-16"
          },
          {
            "value": "Credit Card Expense",
            "id": "144"
          },
          {
            "value": ""
          },
          {
            "value": "",
            "id": ""
          },
          {
            "value": ""
          },
          {
            "value": "Mastercard",
            "id": "41"
          },
          {
            "value": ""
          },
          {
            "value": "34.00"
          }
        ],
        "type": "Data"
      },
      {
        "ColData": [
          {
            "value": "0-00-00"
          },
          {
            "value": "",
            "id": "144"
          },
          {
            "value": ""
          },
          {
            "value": "",
            "id": ""
          },
          {
            "value": ""
          },
          {
            "value": "Automobile",
            "id": "55"
          },
          {
            "value": "34.00"
          },
          {
            "value": ""
          }
        ],
        "type": "Data"
      },
      {
        "Summary": {
          "ColData": [
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "34.00"
            },
            {
              "value": "34.00"
            }
          ]
        },
        "type": "Section"
      },
      {
        "ColData": [
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          },
          {
            "value": ""
          }
        ],
        "type": "Data"
      },
      {
        "Summary": {
          "ColData": [
            {
              "value": "TOTAL"
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": ""
            },
            {
              "value": "953.99"
            },
            {
              "value": "953.99"
            }
          ]
        },
        "type": "Section"
      }
    ]
  }
}
export default JournalReport