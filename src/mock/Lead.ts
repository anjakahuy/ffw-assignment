const Leads = {
  "totalSize": 25,
  "done": true,
  "records": [
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoDEAS"
          },
          "Id": "00Q5g000005RnoDEAS",
          "LastName": "Boxer",
          "FirstName": "Bertha",
          "Title": "Director of Vendor Relations",
          "Company": "Farmers Coop. of Florida",
          "Phone": "(850) 644-4200",
          "Email": "bertha@fcof.net",
          "LeadSource": "Web",
          "Status": "Working - Contacted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoEEAS"
          },
          "Id": "00Q5g000005RnoEEAS",
          "LastName": "Cotton",
          "FirstName": "Phyllis",
          "Title": "CFO",
          "Company": "Abbott Insurance",
          "Phone": "(703) 757-1000",
          "Email": "pcotton@abbottins.net",
          "LeadSource": "Web",
          "Status": "Open - Not Contacted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoFEAS"
          },
          "Id": "00Q5g000005RnoFEAS",
          "LastName": "Glimpse",
          "FirstName": "Jeff",
          "Title": "SVP, Procurement",
          "Company": "Jackson Controls",
          "Phone": "886-2-25474189",
          "Email": "jeffg@jackson.com",
          "LeadSource": "Phone Inquiry",
          "Status": "Open - Not Contacted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoGEAS"
          },
          "Id": "00Q5g000005RnoGEAS",
          "LastName": "Braund",
          "FirstName": "Mike",
          "Title": "VP, Technology",
          "Company": "Metropolitan Health Services",
          "Phone": "(410) 381-2334",
          "Email": "likeb@metro.com",
          "LeadSource": "Purchased List",
          "Status": "Open - Not Contacted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoHEAS"
          },
          "Id": "00Q5g000005RnoHEAS",
          "LastName": "Feager",
          "FirstName": "Patricia",
          "Title": "CEO",
          "Company": "International Shipping Co.",
          "Phone": "(336) 777-1970",
          "Email": "patricia_feager@is.com",
          "LeadSource": "Partner Referral",
          "Status": "Working - Contacted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoIEAS"
          },
          "Id": "00Q5g000005RnoIEAS",
          "LastName": "Mcclure",
          "FirstName": "Brenda",
          "Title": "CFO",
          "Company": "Cadinal Inc.",
          "Phone": "(847) 262-5000",
          "Email": "brenda@cardinal.net",
          "LeadSource": "Web",
          "Status": "Working - Contacted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoJEAS"
          },
          "Id": "00Q5g000005RnoJEAS",
          "LastName": "Maccleod",
          "FirstName": "Violet",
          "Title": "VP, Finance",
          "Company": "Emerson Transport",
          "Phone": "(770) 395-2370",
          "Email": "violetm@emersontransport.com",
          "LeadSource": "Phone Inquiry",
          "Status": "Working - Contacted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoKEAS"
          },
          "Id": "00Q5g000005RnoKEAS",
          "LastName": "Snyder",
          "FirstName": "Kathy",
          "Title": "Regional General Manager",
          "Company": "TNR Corp.",
          "Phone": "(860) 273-0123",
          "Email": "ksynder@tnr.net",
          "LeadSource": "Purchased List",
          "Status": "Working - Contacted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoLEAS"
          },
          "Id": "00Q5g000005RnoLEAS",
          "LastName": "James",
          "FirstName": "Tom",
          "Title": "SVP, Production",
          "Company": "Delphi Chemicals",
          "Phone": "(952) 346-3500",
          "Email": "tom.james@delphi.chemicals.com",
          "LeadSource": "Web",
          "Status": "Working - Contacted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoMEAS"
          },
          "Id": "00Q5g000005RnoMEAS",
          "LastName": "Brownell",
          "FirstName": "Shelly",
          "Title": "SVP, Technology",
          "Company": "Western Telecommunications Corp.",
          "Phone": "(408) 326-9000",
          "Email": "shellyb@westerntelecom.com",
          "LeadSource": "Partner Referral",
          "Status": "Working - Contacted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoNEAS"
          },
          "Id": "00Q5g000005RnoNEAS",
          "LastName": "Owenby",
          "FirstName": "Pamela",
          "Title": "SVP, Technology",
          "Company": "Hendrickson Trading",
          "Phone": "(570) 326-1571",
          "Email": "pam_owenby@hendricksontrading.com",
          "LeadSource": "Partner Referral",
          "Status": "Closed - Not Converted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoOEAS"
          },
          "Id": "00Q5g000005RnoOEAS",
          "LastName": "May",
          "FirstName": "Norm",
          "Title": "VP, Facilities",
          "Company": "Greenwich Media",
          "Phone": "(419) 289-3555",
          "Email": "norm_may@greenwich.net",
          "LeadSource": "Web",
          "Status": "Working - Contacted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoPEAS"
          },
          "Id": "00Q5g000005RnoPEAS",
          "LastName": "Stumuller",
          "FirstName": "Pat",
          "Title": "SVP, Administration and Finance",
          "Company": "Pyramid Construction Inc.",
          "Phone": "33562156600",
          "Email": "pat@pyramid.net",
          "LeadSource": "Phone Inquiry",
          "Status": "Closed - Converted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoQEAS"
          },
          "Id": "00Q5g000005RnoQEAS",
          "LastName": "Young",
          "FirstName": "Andy",
          "Title": "SVP, Operations",
          "Company": "Dickenson plc",
          "Phone": "(620) 241-6200",
          "Email": "a_young@dickenson.com",
          "LeadSource": "Purchased List",
          "Status": "Closed - Converted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoREAS"
          },
          "Id": "00Q5g000005RnoREAS",
          "LastName": "Akin",
          "FirstName": "Kristen",
          "Title": "Director, Warehouse Mgmt",
          "Company": "Aethna Home Products",
          "Phone": "(434) 369-3100",
          "Email": "kakin@athenahome.com",
          "LeadSource": "Partner Referral",
          "Status": "Working - Contacted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoSEAS"
          },
          "Id": "00Q5g000005RnoSEAS",
          "LastName": "Monaco",
          "FirstName": "David",
          "Title": "CFO",
          "Company": "Blues Entertainment Corp.",
          "Phone": "(033) 452-1299",
          "Email": "david@blues.com",
          "LeadSource": "Purchased List",
          "Status": "Working - Contacted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoTEAS"
          },
          "Id": "00Q5g000005RnoTEAS",
          "LastName": "Crenshaw",
          "FirstName": "Carolyn",
          "Title": "VP, Technology",
          "Company": "Ace Iron and Steel Inc.",
          "Phone": "(251) 679-2200",
          "Email": "carolync@aceis.com",
          "LeadSource": "Phone Inquiry",
          "Status": "Closed - Not Converted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoUEAS"
          },
          "Id": "00Q5g000005RnoUEAS",
          "LastName": "Rogers",
          "FirstName": "Jack",
          "Title": "VP, Facilities",
          "Company": "Burlington Textiles Corp of America",
          "Phone": "(336) 222-7000",
          "Email": "jrogers@btca.com",
          "LeadSource": "Web",
          "Status": "Closed - Converted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoVEAS"
          },
          "Id": "00Q5g000005RnoVEAS",
          "LastName": "Dadio Jr",
          "FirstName": "Bill",
          "Title": "CFO",
          "Company": "Zenith Industrial Partners",
          "Phone": "(614) 431-5000",
          "Email": "bill_dadio@zenith.com",
          "LeadSource": "Web",
          "Status": "Closed - Not Converted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoWEAS"
          },
          "Id": "00Q5g000005RnoWEAS",
          "LastName": "Luce",
          "FirstName": "Eugena",
          "Title": "CEO",
          "Company": "Pacific Retail Group",
          "Phone": "(781) 270-6500",
          "Email": "eluce@pacificretail.com",
          "LeadSource": "Purchased List",
          "Status": "Closed - Not Converted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoXEAS"
          },
          "Id": "00Q5g000005RnoXEAS",
          "LastName": "Eberhard",
          "FirstName": "Sandra",
          "Title": "VP, Production",
          "Company": "Highland Manufacturing Ltd.",
          "Phone": "(626) 440-0700",
          "Email": "sandra_e@highland.net",
          "LeadSource": "Purchased List",
          "Status": "Working - Contacted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005RnoYEAS"
          },
          "Id": "00Q5g000005RnoYEAS",
          "LastName": "Bair",
          "FirstName": "Betty",
          "Title": "VP, Administration",
          "Company": "American Banking Corp.",
          "Phone": "(610) 265-9100",
          "Email": "bblair@abankingco.com",
          "LeadSource": "Purchased List",
          "Status": "Working - Contacted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005BezFEAS"
          },
          "Id": "00Q5g000005BezFEAS",
          "LastName": "Nguyen",
          "FirstName": "Huy",
          "Title": null,
          "Company": "Lotte",
          "Phone": null,
          "Email": null,
          "LeadSource": "Web",
          "Status": "Closed - Converted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005BezGEAS"
          },
          "Id": "00Q5g000005BezGEAS",
          "LastName": "Nguyen",
          "FirstName": "Ha",
          "Title": null,
          "Company": "Toshiba",
          "Phone": null,
          "Email": null,
          "LeadSource": null,
          "Status": "Working - Contacted"
      },
      {
          "attributes": {
              "type": "Lead",
              "url": "/services/data/v42.0/sobjects/Lead/00Q5g000005BezHEAS"
          },
          "Id": "00Q5g000005BezHEAS",
          "LastName": "Shady",
          "FirstName": "Lil",
          "Title": null,
          "Company": "Vin",
          "Phone": null,
          "Email": null,
          "LeadSource": null,
          "Status": "Open - Not Contacted"
      }
  ]
}
export default Leads