import { configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import fontTabsSlice from "../modules/fontTab/reducers";
import fontTabsSaga from "../modules/fontTab/saga";

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
  reducer: {
    fontTabs: fontTabsSlice,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().prepend(sagaMiddleware),
});
sagaMiddleware.run(fontTabsSaga);

export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
