/* eslint-disable react/prop-types */
import * as React from "react";

function renderHeaderForRow(columns) {
  if (columns && columns.length > 0) {
    var res = columns.map((item, index) => {
      return `<td key="row-header-${index.value}">${item.value}</td>`;
    });
    return res.join("");
  } else {
    return '';
  }
  
}
function renderDetailForRow(data, level) {
  var html = '';
  if (data.Header) {
    html += `<tr class="header-col level-${level}">` + renderHeaderForRow(data.Header.ColData) + '</tr>';
  }
  if (data.Rows && data.Rows.Row) {
    data.Rows.Row.forEach(item => {
      if (item.Rows) {
        html += renderDetailForRow(item, level + 1)
      } else {
        const sumary = item.Summary ? item.Summary.ColData : item.ColData;
        const className = item.Summary ? 'header-col' : 'data-col';
        html += `<tr class="${className} level-${level}"">` + renderHeaderForRow(sumary) + '</tr>';
      } 
    })
  }

  if (data.Summary) {
    html += `<tr class="sumary-col level-${level}"">` + renderHeaderForRow(data.Summary.ColData) + '</tr>';
  }
  return html
}
export default class ReportIntuitComponent extends React.Component {

  render() {
    const { Header, Columns, Rows } = this.props.data;
    console.log(Rows)
    const RowDetail = renderDetailForRow({Rows: Rows}, 0);
    return (
      <div className="container-fluid">
        <h2 className="text-center">FFW Vietnam Intuit</h2>
        <h4 className="text-center">{Header.ReportName}</h4>
        <p className="text-center">
          From {Header.StartPeriod} to {Header.EndPeriod}
        </p>
        <table className="table">
          <thead>
            <tr>
              {Columns.Column.map(
                (column, index) => {
                  return <th key={index + "key"}>{column.ColTitle}</th>;
                }
              )}
            </tr>
          </thead>
          <tbody dangerouslySetInnerHTML={{__html: RowDetail}} />
         
        </table>
      </div>
    );
  }
}
