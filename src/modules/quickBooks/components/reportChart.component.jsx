import React from "react";
import Chart from "react-google-charts";

function ReportChartComponent() {
  return (
    <>
    <div className="google-chart">
      <Chart
        width={"1200px"}
        height={"600px"}
        chartType="ComboChart"
        loader={<div>Loading Chart</div>}
        data={[
          [
            "Month",
            "Revenue",
            "Cost of Sales",
            "Admin cost",
            "Total cost",
            "EBITDA",
            'Testing-line'
          ],
          ["2021/01", 10000, 4000, 2000, 6000, 4000, 1614.6],
          ["2021/02", 11000, 3000, 2000, 5000, 6000, 3682],
          ["2021/03", 12000, 3000, 2000, 5000, 7000, 4623],
          ["2021/04", 13000, 4000, 2000, 6000, 7000, 5609.4],
          ["2021/05", 14000, 4000, 2000, 6000, 8000, 4569.6],
          ["2021/06", 8000, 3000, 1500, 4500, 3500, 5569.6],
        ]}
        options={{
          title: "Budget for 2021",
          vAxis: { title: "Budget ($)" },
          hAxis: { title: "Month" },
          seriesType: "bars",
          series: { 5: { type: "line" } },
        }}
        rootProps={{ "data-testid": "1" }}
      />
      </div>
    </>
  );
}

export default ReportChartComponent;
