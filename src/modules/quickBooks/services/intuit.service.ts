import axios from "axios";
axios.defaults.baseURL = "https://sandbox-quickbooks.api.intuit.com/v3/company/4620816365172812330";
axios.interceptors.request.use(function (config) {
  const token = 'eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..gExDGUklhBkgij6fK9JwIA.ywWR-lH2LAAtG8Y-x5m5X2B-9jf2puO48JNozz-fLHDEMxLEJ9uzIiFc9qIE6Bpv4afQU7oemJRm19Q3hZgTN7FuwVezDawtn6GezgKbkIVOV2xDnYgTaVJV0vgkQUs_c3WLd6LzqSZ8wUH-X0fWUPEIVbXEQOEVCbx_dtDwHRsXoAeWxOtQ_OwzsjUUZ-iaISt1w655v2EphE7-WGQgxXSqdHMgLTsXd41gdgIeWjCPu6O5SIx4xEMns1-pOM9HWDA_75Cnhq1RIW9y6CrByWjQuoqFjjZRCxadJVGXGXBn1wP-pYEJjjpQp9cGfjLEsDoG8vyEtcVbb9X7nOyRtFrT-3B_7daH-FFCiz7ygEdMn7w1yTvtvtrs_dY_XJXkvXqBVnXzvKjkmS9D43DRqj7z61cd_neIkvLqQhh7WNWqaPKfPtSwzuU6A1rxGifSEVx_ocO4WQSCnYjq5NfqFVLReChW6ThgegjhCWzDwg5ghNir1x7xAd4wpJE0yF6aLYG125HyY84Yeiym_pYUiGfOo9nXVaZTwpaGk-LHw5poz9gFEIZdsYIVRzkR65xaIVkrJbNMYKrKaXy7OoVWRCyRXREgRbb__ZLB3LyOiJQgZV0kBGZSa2AHIB3ushue7iUSvOUxyZgywptyzEHV9efondxjZHXzFBbpj64v8M0ejlx-e0s_rfVsxxia6F6zlznmFB8Y0uvG-Bo0LKGB8R_rLIbAD0rWC7uA0u3E1GZl3RJd_NLzfkOdFJgKquld.MrcZHPUZizBINH6m4mRMlQ';
  config.headers.Authorization =  `Bearer ${token}`;

  return config;
});
class IntuitService {
  async getItems(path: string): Promise<any> {
    const res = await axios.get(`/${path}`);
    return res.data;
  }
}

export default new IntuitService();
