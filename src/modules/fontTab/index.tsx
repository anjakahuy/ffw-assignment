import React, { Suspense } from "react";
import { connect } from "react-redux";
import { RootState } from "../../store";
import TabsComponent from "./components/tabs.component";

const MyFontsComponent = React.lazy(
  () => import("./components/myFonts.component")
);
const BuyFontsComponent = React.lazy(
  () => import("./components/buyFonts.component")
);

export interface Props {
  currentTab: number;
}

export interface State {
  loading: boolean;
  mapComponents: { [key: number]: any };
}

class FontTabsComponent extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      loading: true,
      mapComponents: {
        101: <MyFontsComponent />,
        102: <BuyFontsComponent />,
      },
    };
  }
  
  render() {
    return (
      <>
        <div className="wrap-container">
          <TabsComponent />
          <Suspense fallback={<div>Loading...</div>}>
            {this.state.mapComponents[this.props.currentTab]}
          </Suspense>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  const { currentTab } = state.fontTabs;
  return {
    currentTab,
  };
};


export default connect(mapStateToProps, {})(FontTabsComponent);
