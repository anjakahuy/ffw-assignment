export interface Tab {
  id: number;
  label: string;
  content_enpoint: string;
}

export interface Font {
  id: number;
  abbr: string;
  color: string;
  'color-blind-label': string;
  label: string;
}

export interface State {
  tabs: Tab[];
  fonts: Font[];
  buyContent: string;
  currentTab: number;
  selectedFont: number;
}
