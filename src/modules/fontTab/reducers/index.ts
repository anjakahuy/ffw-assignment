import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Font, State, Tab } from "../types";
var initialState: State = {
  tabs: [],
  fonts: [],
  buyContent: "",
  currentTab: 101,
  selectedFont: 112,
};

export const fontTabsSlice = createSlice({
  name: "fontTabs",
  initialState,
  reducers: {
    setTabs: (state, action: PayloadAction<{ data: Tab[] }>) => {
      state.tabs = action.payload.data;
    },
    setFonts: (state, action: PayloadAction<{ data: Font[] }>) => {
      state.fonts = action.payload.data;
    },
    setBuyContent: (state, action: PayloadAction<{ data: string }>) => {
      state.buyContent = action.payload.data;
    },
    setCurrentTab: (state, action: PayloadAction<{ data: number }>) => {
      state.currentTab = action.payload.data;
    },
    setSelectedFont: (state, action: PayloadAction<{ data: number }>) => {
      state.selectedFont = action.payload.data;
    },
  },
});

export default fontTabsSlice.reducer;
