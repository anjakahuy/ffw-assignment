export const GET_TABS = 'fontTabs/getTabs'
export const GET_FONTS = 'fontTabs/getFonts'
export const GET_BUY_CONTENT = 'fontTabs/getBuyContent'

export const SET_TABS = 'fontTabs/setTabs'
export const SET_FONTS = 'fontTabs/setFonts'
export const SET_BUY_CONTENT = 'fontTabs/setBuyContent'
export const SET_CURENT_TAB = 'fontTabs/setCurrentTab'
export const SET_SELECTED_FONT = 'fontTabs/setSelectedFont'