import { put, takeEvery, all, call } from "redux-saga/effects";
import * as actions from "../actions";
import FontTabApiService from "../services/fontTabApi.service";
import { Font, Tab } from "../types";

function* getTabs() {
  const data: Tab[] = yield call(FontTabApiService.getItems, "tabs");
  yield put({ type: actions.SET_TABS, payload: { data } });
}

function* getBuyContent() {
  const data: { content: string } = yield call(
    FontTabApiService.getItems,
    "fonts_b"
  );
  yield put({ type: actions.SET_BUY_CONTENT, payload: { data: data.content } });
}

function* getFonts() {
  const data: { content: Font[] } = yield call(
    FontTabApiService.getItems,
    "fonts_a"
  );
  yield put({ type: actions.SET_FONTS, payload: { data: data.content } });
}

function* watchAsync() {
  yield takeEvery(actions.GET_TABS, getTabs);
  yield takeEvery(actions.GET_BUY_CONTENT, getBuyContent);
  yield takeEvery(actions.GET_FONTS, getFonts);
}

export default function* fontTabsSaga() {
  yield all([watchAsync()]);
}
