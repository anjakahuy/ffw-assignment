import axios from "axios";
axios.defaults.baseURL = "http://json.ffwagency.md/";

class FontTabApiService {
  async getItems(path: string): Promise<any> {
    const res = await axios.get(`/${path}`);
    return res.data;
  }
}

export default new FontTabApiService();
