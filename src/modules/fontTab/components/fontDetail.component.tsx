import * as React from "react";
import { Font } from "../types";

export interface Props {
  font: Font;
  selected: boolean;
}

class FontDetailComponent extends React.Component<Props, {}> {
  render() {
    const font = this.props.font;
    const cardColor = {
      background: font.color,
    };
    return (
      <>
        <div className="card-color">
          <div className="card-abbr" style={cardColor}>
            {this.props.font.abbr}
          </div>
        </div>
        <div className="card-detail">
          <ul>
            <li>{this.props.font.label}</li>
          </ul>
        </div>
      </>
    );
  }
}

export default FontDetailComponent;
