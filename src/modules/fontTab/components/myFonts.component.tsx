import * as React from "react";
import { connect } from "react-redux";
import { AppDispatch, RootState } from "../../../store";
import * as actions from "../actions";
import { Font } from "../types";
import FontDetailComponent from "./fontDetail.component";

export interface Props {
  fonts: Font[];
  getFonts: any;
  selectedFont: number;
  setSelectedFont: any;
}

class MyFontsComponent extends React.Component<Props, {}> {
  activeFont: any;
  constructor(props: Props) {
    super(props);
    this.handleKeyup = this.handleKeyup.bind(this);
    this.handlePreviousFont = this.handlePreviousFont.bind(this);
    this.handleNextFont = this.handleNextFont.bind(this);
  }

  /**
   * Get and set fonts to store when initializing
   */
  componentDidMount() {
    if (this.props.fonts.length === 0) {
      this.props.getFonts();
    }
  }

  /**
   * Focusing on the selected font
   */
  componentDidUpdate() {
    if (this.activeFont) {
      this.activeFont.focus();
    }
  }

  /**
   * Set selected font to previous item 
   * @param index : number
   * @return void
   */
  handlePreviousFont(index: number) {
    if (index > 0) this.handleSelectFont(this.props.fonts[index - 1].id);
  }

  /**
   * Set selected font to next item 
   * @param index : number
   * @return void
   */
  handleNextFont(index: number) {
    if (index < this.props.fonts.length - 1)
      this.handleSelectFont(this.props.fonts[index + 1].id);
  }

  /**
   * Handle keyboard events
   * @param e 
   * @param fontId: number
   * @param index: number
   * @return void
   */
  handleKeyup(e: any, fontId: number, index: number) {
    e.preventDefault();
    if (e.which === 13) this.handleSelectFont(fontId); // Press Enter
    else if (e.which === 37 || e.which === 38) this.handlePreviousFont(index); // Press left or up
    else if (e.which === 39 || e.which === 40) this.handleNextFont(index); // Press right or down
  }

  /**
   * Set selected font to store
   * @param fontId 
   */
  handleSelectFont(fontId: number) {
    if (fontId !== this.props.selectedFont) {
      this.props.setSelectedFont({ data: fontId });
    }
  }

  render() {
    return (
      <div className="my-fonts">
        <div className="flex-container" role="tablist" aria-label="Select font">
          {this.props.fonts.map((item, index) => {
            const selected = this.props.selectedFont === item.id;
            const selectedClass = selected ? "selected card" : "card";
            return (
              <div
                className={selectedClass }
                key={`tab-font-${item.id}`}
                role="tab"
                title={item.label}
                aria-selected={selected}
                aria-controls={`tabFontpanel-${item.id}`}
                aria-label={item['color-blind-label']}
                id={`tab-font-${item.id}`}
                tabIndex={selected ? 0 : -1}
                onClick={() => this.handleSelectFont(item.id)}
                onKeyUp={(e) => this.handleKeyup(e, item.id, index)}
                ref={(font) => {
                  if (selected) this.activeFont = font;
                }}
              >
                <FontDetailComponent selected={selected} font={item} />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  const { fonts, selectedFont } = state.fontTabs;
  return {
    fonts,
    selectedFont,
  };
};

const mapDispatchToProps = (dispatch: AppDispatch) => {
  return {
    getFonts: () => dispatch({ type: actions.GET_FONTS }),
    setSelectedFont: (payload: { data: number }) =>
      dispatch({ type: actions.SET_SELECTED_FONT, payload }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyFontsComponent);
