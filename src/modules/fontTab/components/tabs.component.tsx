import * as React from "react";
import { connect } from "react-redux";
import { AppDispatch, RootState } from "../../../store";
import * as actions from "../actions";
import { Tab } from "../types";

export interface Props {
  tabs: Tab[];
  currentTab: number;
  setCurrentTab: any;
  getTabs: any;
}

class TabsComponent extends React.Component<Props, { activeLink: any }> {
  activeTab: any;
  constructor(props: Props) {
    super(props);
    this.handleKeyup = this.handleKeyup.bind(this);
    this.handlePreviousTab = this.handlePreviousTab.bind(this);
    this.handleNextTab = this.handleNextTab.bind(this);
  }

  /**
   * Set current tab to previous item 
   * @param index : number
   * @return void
   */
  handlePreviousTab(index: number) {
    if (index > 0) this.handleChangeTab(this.props.tabs[index - 1].id);
  }

  /**
   * Set current tab to next item 
   * @param index : number
   * @return void
   */
  handleNextTab(index: number) {
    if (index < this.props.tabs.length - 1)
      this.handleChangeTab(this.props.tabs[index + 1].id);
  }

  /**
   * Handle keyboard events
   * @param e 
   * @param tabId: number
   * @param index: number
   * @return void
   */
  handleKeyup(e: any, tabId: number, index: number) {
    e.preventDefault();
    if (e.which === 13) this.handleChangeTab(tabId); // Press enter
    else if (e.which === 37) this.handlePreviousTab(index); // Press left
    else if (e.which === 39) this.handleNextTab(index); // Press right
  }

  /**
   * Set selected font to store
   * @param fontId 
   */
  handleChangeTab(tabId: number): void {
    if (tabId !== this.props.currentTab) {
      this.props.setCurrentTab({ data: tabId });
    }
  }

  /**
   * Focusing on the current tab
   */
  componentDidUpdate() {
    this.activeTab.focus();
  }

  /**
   * Get and set fonts to store when initializing
   */
  componentDidMount() {
    this.props.getTabs();
  }

  render() {
    return (
      <div className="tabs">
        <h3 className="tab-title">Please select one font</h3>
        <ul className="tab-list" role="tablist" aria-label="Select menu">
          {this.props.tabs.map((item, index) => {
            const selected = item.id === this.props.currentTab;
            return (
              <li
                role="tab"
                aria-selected={selected}
                aria-controls={`tabpanel-${item.id}`}
                aria-label={item.label}
                id={`tab-${item.id}`}
                key={`tab-${item.id}`}
                tabIndex={selected ? 0 : -1}
                className={selected ? "active" : ""}
                onClick={() => this.handleChangeTab(item.id)}
                onKeyUp={(e) => this.handleKeyup(e, item.id, index)}
                ref={(tab) => {
                  if (selected) this.activeTab = tab;
                }}
              >
                <span>{item.label}</span>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  const { tabs, currentTab } = state.fontTabs;
  return {
    tabs,
    currentTab,
  };
};

const mapDispatchToProps = (dispatch: AppDispatch) => {
  return {
    getTabs: () => dispatch({ type: actions.GET_TABS }),
    setCurrentTab: (payload: { data: number }) =>
      dispatch({ type: actions.SET_CURENT_TAB, payload }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TabsComponent);
