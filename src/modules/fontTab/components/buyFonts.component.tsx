import * as React from "react";
import { connect } from "react-redux";
import { AppDispatch, RootState } from "../../../store";
import * as actions from "../actions";

interface Props {
  buyContent: string;
  getBuyFonts: any;
}

class BuyFontsComponent extends React.Component<Props, {}> {

  /**
   * Get and set content to store when initializing
   */
  componentDidMount() {
    if (this.props.buyContent === "" || this.props.buyContent === null) {
      this.props.getBuyFonts();
    }
  }

  render() {
    return (
      <div className="buy-fonts">
        <div className="buy-fonts-content">{this.props.buyContent}</div>
      </div>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  const { buyContent } = state.fontTabs;
  return {
    buyContent,
  };
};

const mapDispatchToProps = (dispatch: AppDispatch) => {
  return {
    getBuyFonts: () => dispatch({ type: actions.GET_BUY_CONTENT }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BuyFontsComponent);
