import React from 'react'
import { Meta, Story } from '@storybook/react';
import Button, { Props } from './Button';

export default {
  component: Button,
  title: 'Components/Button',
} as Meta;

//👇 We create a “template” of how args map to rendering
const Template: Story<Props> = (args) => <Button {...args} />;

export const Primary = Template.bind({});

Primary.args = {
  primary: true,
  label: 'Button',
};

export const Secondary = Template.bind({});

Secondary.args = {
  label: 'Second',
};