import React, { Component } from 'react'

export interface Props {
  label: string,
  primary?: boolean
}
interface State {

}

class Button extends Component<Props, State> {
  render() {
    const primaryStyle = this.props.primary ? 'btn-primary' : ''
    return (
      <button className={`btn ${primaryStyle}`}>{this.props.label}</button>
    )
  }
}

export default Button