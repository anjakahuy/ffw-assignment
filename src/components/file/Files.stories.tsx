import React from 'react'
import { Meta, Story } from '@storybook/react';
import Files, { FilesProps } from './Files';
import { sampleFiles } from './mock'
export default {
  component: Files,
  title: 'Components/Files',
} as Meta;

//👇 We create a “template” of how args map to rendering
const Template: Story<FilesProps> = (args) => <Files {...args} />;

export const Initial = Template.bind({});

Initial.args = {

};


export const HasUploadedFiles = Template.bind({});

HasUploadedFiles.args = {
  files: sampleFiles,
};
HasUploadedFiles.storyName = 'If there are existed files';