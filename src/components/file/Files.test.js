import React from "react";
import { fireEvent, render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { Initial, HasUploadedFiles } from "./Files.stories";
import { sampleFiles } from './mock'
const file = new File([new ArrayBuffer(1)], 'file.jpg');

it("renders only dropzone in initial",  () => {
  render(<Initial />);

  const fileList = screen.queryByTestId('file-list')
  expect(fileList).toBe(null)

  const fileUpload = screen.getByTestId('file-upload')
  expect(fileUpload).toBeInTheDocument()
});

it("renders dropzone and file list if already existed files",  () => {
  render(<HasUploadedFiles files={sampleFiles}/>);

  const fileList = screen.queryByTestId('file-list')
  expect(fileList).toBeInTheDocument()

  const fileItems = screen.getAllByTestId('file-item')
  expect(fileItems).toHaveLength(3)

  const fileUpload = screen.getByTestId('file-upload')
  expect(fileUpload).toBeInTheDocument()

});

test("upload file in initial",  () => {
  render(<Initial />);

  // file-list is not rendered
  var fileList = screen.queryByTestId('file-list')
  expect(fileList).toBe(null)

  
  const fileInput = screen.getByTestId('file-input');
  fireEvent.change(fileInput, {target: {files: [file]}})

  // File list is rendered
  fileList = screen.queryByTestId('file-list')
  expect(fileList).toBeInTheDocument()

  // Number of files is 1
  var fileItems = screen.getAllByTestId('file-item')
  expect(fileItems).toHaveLength(1)

});

test("upload file when already existed files",  () => {
  render(<HasUploadedFiles files={sampleFiles}/>);

  // File list is rendered
  var fileList = screen.queryByTestId('file-list')
  expect(fileList).toBeInTheDocument()

  const fileInput = screen.getByTestId('file-input');
  fireEvent.change(fileInput, {target: {files: [file]}})

  // Number of files is 4
  var fileItems = screen.getAllByTestId('file-item')
  expect(fileItems).toHaveLength(4)

});

test('upload with drag and drop', () => {
  render(<Initial />);
  // file-list is not rendered
  var fileList = screen.queryByTestId('file-list')
  expect(fileList).toBe(null)

  const fileDragDrop = screen.queryByTestId('file-drag-drop')
  fireEvent.drop(fileDragDrop, {dataTransfer: {
    files: [file]
  }})

  fileList = screen.queryByTestId('file-list')
  expect(fileList).toBeInTheDocument()

  var fileItems = screen.getAllByTestId('file-item')
  expect(fileItems).toHaveLength(1)

})
