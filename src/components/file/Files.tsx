/* eslint-disable no-restricted-globals */
import React, { Component } from "react";
import style from "./file.module.scss";

export interface FilesProps {
  files?: File[];
  acceptedFiles?: string[];
  maxFileSize?: number;
}
export interface File {
  id: number;
  name: string;
  ext: string;
  size: number;
}
export interface FilesState {
  files: File[];
  dragging: boolean;
}
export default class Files extends Component<FilesProps, FilesState> {
  private fileRef: React.RefObject<HTMLInputElement>;
  constructor(props: FilesProps) {
    super(props);
    this.state = { files: props.files ? props.files : [], dragging: false };
    this.fileRef = React.createRef();
    this.handleClickUpload = this.handleClickUpload.bind(this);
    this.handleUploadFiles = this.handleUploadFiles.bind(this);
    this.onFileDrop = this.onFileDrop.bind(this);
    this.onDragEnter = this.onDragEnter.bind(this);
    this.onDragOver = this.onDragOver.bind(this)
    this.onDragLeave = this.onDragLeave.bind(this)
  }

  onDragLeave(event: any) {
    event.preventDefault();
    this.setState({ dragging: false });
  }

  onDragEnter(event: any) {
    event.preventDefault();
    this.setState({ dragging: true });
  }

  onDragOver(event: any) {
    event.preventDefault();
  }

  onFileDrop(event: any) {
    event.preventDefault();
    const files = event.dataTransfer.files;
    this.handleAddFilesToList(files)
  }

  handleClickUpload(): void {
    if (this.fileRef.current) {
      this.fileRef.current.click();
    }
  }

  handleAddFilesToList(files: FileList ) {
    const uploaded = Array.from(files).map((file) => {
      const { name, size, type } = file;
      return {
        id: 1,
        name,
        ext: type,
        size,
      };
    });
    this.setState((state) => {
      return { 
        ...state,
        files: state.files.concat(uploaded)
      }
    });
  }
  handleUploadFiles(): void {
    if (
      this.fileRef.current &&
      this.fileRef.current.files &&
      this.fileRef.current.files.length > 0
    ) {
      this.handleAddFilesToList(this.fileRef.current.files)
    }
  }
  handleDelete(id: number): void {}
  componentDidMount() {
    window.addEventListener("dragover",function(e){
      e = e || event;
      e.preventDefault();
    },false);
    window.addEventListener("drop",function(e){
      e = e || event;
      e.preventDefault();
    },false);
  }
  render() {
    return (
      <div className="files-container">
        {this.state.files.length > 0 && (
          <div className="file-list" data-testid="file-list">
            <table className="table">
              <thead>
                <tr>
                  <th></th>
                  <th>Filename</th>
                  <th>File extension</th>
                  <th>File size</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {this.state.files.map((file, index) => {
                  return (
                    <tr key={index} data-testid="file-item">
                      <td></td>
                      <td>{file.name}</td>
                      <td>{file.ext}</td>
                      <td>{file.size}</td>
                      <td>
                        <button
                          className="btn btn-primary"
                          onClick={() => this.handleDelete(file.id)}
                        >
                          Delete
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        )}
        <div className="file-upload" data-testid="file-upload">
          <div
            className={`${style.fileUploadInner} ${this.state.dragging ? style.dragging : ''} d-flex justify-content-center align-items-center`}
            draggable="true"
            onClick={this.handleClickUpload}
            onDrop={this.onFileDrop}
            onDragEnter={this.onDragEnter}
            onDragOver={this.onDragOver}
            onDragLeave={this.onDragLeave}
            data-testid="file-drag-drop"
            
          >
            Click or Drag & Drop to upload file
            <input
              className="d-none"
              onChange={this.handleUploadFiles}
              data-testid="file-input"
              ref={this.fileRef}
              type="file"
              name="file_upload"
              id="file_upload"
              multiple
            />
          </div>
        </div>
        <div className="file-buttons"></div>
      </div>
    );
  }
}
