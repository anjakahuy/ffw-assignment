import React, { Component } from "react";

class Table extends Component {
  render() {
    return (
      <table className="table" id="table">
        <thead>
          <tr>
            <th data-type="number">No.</th>
            <th>First name</th>
            <th>Last name</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className="draggable">2</td>
            <td>Penelope</td>
            <td>Mills</td>
          </tr>
          <tr>
            <td className="draggable">7</td>
            <td>Michelle</td>
            <td>King</td>
          </tr>
          <tr>
            <td className="draggable">3</td>
            <td>Sarah</td>
            <td>Grant</td>
          </tr>
          <tr>
            <td className="draggable">1</td>
            <td>Andrea</td>
            <td>Ross</td>
          </tr>
          <tr>
            <td className="draggable">4</td>
            <td>Vanessa</td>
            <td>Roberts</td>
          </tr>
          <tr>
            <td className="draggable">5</td>
            <td>Oliver</td>
            <td>Alsop</td>
          </tr>
          <tr>
            <td className="draggable">6</td>
            <td>Jennifer</td>
            <td>Forsyth</td>
          </tr>
          <tr>
            <td className="draggable">8</td>
            <td>Steven</td>
            <td>Kelly</td>
          </tr>
          <tr>
            <td className="draggable">9</td>
            <td>Julian</td>
            <td>Ferguson</td>
          </tr>
          <tr>
            <td className="draggable">10</td>
            <td>Chloe</td>
            <td>Ince</td>
          </tr>
        </tbody>
      </table>
    );
  }
}

export default Table;
