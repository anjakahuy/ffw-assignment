/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from "react";
import "../../App.scss";
import ReportIntuitComponent from "../../modules/quickBooks/components/reportIntuit.component";
import ReportChartComponent from '../../modules/quickBooks/components/reportChart.component';
import ProfitAndLossDetail from "../../mock/ProfitAndLossDetail";
import CustomerBalanceDetail from "../../mock/Clients";
import TransactionList from "../../mock/TransactionList";
import JournalReport from "../../mock/JournalReport";

const data: {[key: number]: any} = {
  1: ProfitAndLossDetail,
  2: CustomerBalanceDetail,
  3: TransactionList,
  4: JournalReport
}
function QuickBooks() {
  const [currentTab, setTab] = useState(0)
  const [showMenu, toggleMenu] = useState(false)
  function handleClick(id: number) {
    setTab(id)
    toggleMenu(false)
  }

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <a className="navbar-brand" href="#" onClick={() => setTab(0)}>
          QuickBooks Dashboard
        </a>
        <button
          onClick={() => toggleMenu(!showMenu)}
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className={showMenu ? 'show collapse navbar-collapse' : 'collapse navbar-collapse'} id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
              <a className={currentTab === 1 ? 'active nav-link' : 'nav-link'} href="#" onClick={() => handleClick(1)}>
                Finance
              </a>
            </li>
            <li className="nav-item">
              <a className={currentTab === 2 ? 'active nav-link' : 'nav-link'} href="#" onClick={() => handleClick(2)}>
                Clients
              </a>
            </li>
            <li className="nav-item">
              <a className={currentTab === 3 ? 'active nav-link' : 'nav-link'} href="#" onClick={() => handleClick(3)}>
                Operations
              </a>
            </li>
            <li className="nav-item">
              <a className={currentTab === 4 ? 'active nav-link' : 'nav-link'} href="#" onClick={() => handleClick(4)}>
                People
              </a>
            </li>
          </ul>
        </div>
      </nav>
      {currentTab !== 0 ? (
          <ReportIntuitComponent data={data[currentTab]} />
        ) : (
          <ReportChartComponent />
        )
      }
    </>
  );
}

export default QuickBooks;
