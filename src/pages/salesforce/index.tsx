/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from "react";
import Leads from "../../mock/Lead";
import Chart from "react-google-charts";

function SalesForce() {
  const [currentTab, setTab] = useState(0);
  const [showMenu, toggleMenu] = useState(false);
  function handleClick(id: number) {
    setTab(id);
    toggleMenu(false);
  }
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <a className="navbar-brand" href="#" onClick={() => setTab(0)}>
          SalesForce Dashboard
        </a>
        <button
          onClick={() => toggleMenu(!showMenu)}
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div
          className={
            showMenu
              ? "show collapse navbar-collapse"
              : "collapse navbar-collapse"
          }
          id="navbarNav"
        >
          <ul className="navbar-nav">
            <li className="nav-item">
              <a
                className={currentTab === 1 ? "active nav-link" : "nav-link"}
                href="#"
                onClick={() => handleClick(1)}
              >
                Leads
              </a>
            </li>
          </ul>
        </div>
      </nav>
      {currentTab == 1 ? (
        <div className="container-fluid">
          <table className="table">
            <thead>
              <tr>
                <th>Id</th>
                <th>Title</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Company</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Lead Source</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {Leads.records.map((item) => {
                return (
                  <tr key={item.Id}>
                    <td>{item.Id}</td>
                    <td>{item.Title}</td>
                    <td>{item.FirstName}</td>
                    <td>{item.LastName}</td>
                    <td>{item.Company}</td>
                    <td>{item.Phone}</td>
                    <td>{item.Email}</td>
                    <td>{item.LeadSource}</td>
                    <td>{item.Status}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      ) : (
        <>
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 col-sm-6">
              <Chart
                width={"800px"}
                height={"600px"}
                chartType="PieChart"
                loader={<div>Loading Chart</div>}
                data={[
                  ["Task", "Hours per Day"],
                  ["Working - Contacted", 13],
                  ["Open - Not Contacted", 4],
                  ["Closed - Not Converted", 4],
                  ["Closed - Converted", 4],
                ]}
                options={{
                  title: "Leads status",
                }}
                rootProps={{ "data-testid": "1" }}
              />
            </div>
            <div className="col-12 col-sm-6">
              <Chart
                width={"800px"}
                height={"600px"}
                chartType="PieChart"
                loader={<div>Loading Chart</div>}
                data={[
                  ["Task", "Hours per Day"],
                  ["Web", 8],
                  ["Phone Inquiry ", 4],
                  ["Purchased List ", 7],
                  ["Partner Referral", 4],
                ]}
                options={{
                  title: "Leads Source",
                }}
                rootProps={{ "data-testid": "1" }}
              />
            </div>
          </div>
          </div>
        </>
      )}
    </>
  );
}

export default SalesForce;
