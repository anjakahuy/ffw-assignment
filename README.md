# Installation guide

Install this application in two ways

## Install with Docker

1. Install Docker and Docker Compose from [https://www.docker.com/](https://www.docker.com/)
2. Run `docker-compose up --build -d`
3. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


## Install with Node

1. Install NodeJS from [https://nodejs.org/en//](https://nodejs.org/en/)
2. Run `npm install`
3. Run `npm start`
4. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
